import cv2

NUM_CAMERAS = 46
NUM_POINTS = 6969

import yaml

def opencv_matrix(loader, node):
    mapping = loader.construct_mapping(node, deep=True)
    mat = np.array(mapping["data"])
    mat.resize(mapping["rows"], mapping["cols"])
    return mat

yaml.add_constructor(u"tag:yaml.org,2002:opencv-matrix", opencv_matrix)

if __name__ == '__main__':
	cameras = []
	for i in range(NUM_CAMERAS):
		filename = "reconstruction_%d.yaml" % i
		with open(filename) as f:
		    result = yaml.load(f.read())
		P = result["P"]
		K = result["K"]
		cameras.append((P, K))

	points = []
	for i in range(NUM_POINTS):
		filename = "track_%d.yaml" % i
		point = cv.load(filename, name="point")
		color = cv.load(filename, name="color")
		points.append((point, color))
