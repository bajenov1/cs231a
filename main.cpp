#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <iostream>
#include "opencv2/stitching/detail/motion_estimators.hpp"
#include "CornerDetection.cpp"

#include <opencv2/core.hpp>
//#include <opencv2/sfm.hpp>
//#include <opencv2/viz.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;
//using namespace cv::sfm;


/**
 * @function goodFeaturesToTrack_Demo.cpp
 * @brief Apply Shi-Tomasi corner detector
 */


void printMatrices(Mat M) {
  auto s = M.size();
  cout<<"M has: "<<s.width <<" "<<s.height<<endl;
}


int main( int argc, char** argv )
{
    VideoCapture cap(argv[1]);
    if (!cap.isOpened())
    {
        return -1;
    }

    namedWindow("gray", 1);

    Mat frame;
    Mat gray;
    int frameNumber = 0;
    vector<KeyPoint> k1;
    Mat d1;
    for(;;)
    {
        // Get frame
        cap >> frame;
        if (frame.rows == 0) {
            break;
        }
        cvtColor(frame, gray, CV_BGR2GRAY);

        // Get features
        // From http://stackoverflow.com/questions/27533203/how-do-i-use-sift-in-opencv-3-0-with-c
        //cv::Ptr<Feature2D> f2d = SIFT::create();
        cv::Ptr<Feature2D> f2d = SURF::create();
        //cv::Ptr<Feature2D> f2d = DAISY::create();
        //cv::Ptr<Feature2D> f2d = FastFeatureDetector::create();
        std::vector<cv::KeyPoint> keypoints;
        f2d->detect(gray, keypoints);

        std::vector<cv::KeyPoint> k2;
        Mat d2;
        f2d->detectAndCompute(gray, Mat(), k2, d2);

        Mat pos = (Mat_<double>(4,4) << 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
        // If previous keypoints set.
        if (!k1.empty()) {
            FlannBasedMatcher matcher;
            vector<DMatch> matches;
            matcher.match(d1, d2, matches);

            double max_dist = 0; double min_dist = 100;
            for( int i = 0; i < d1.rows; i++ ) {
                double dist = matches[i].distance;
                if( dist < min_dist ) min_dist = dist;
                if( dist > max_dist ) max_dist = dist;
            }
            std::vector< DMatch > good_matches;
            for( int i = 0; i < d1.rows; i++ ) {
                if( matches[i].distance <= max(2*min_dist, 0.02) ){
                    good_matches.push_back( matches[i]);
                }
            }
            vector<Point2f> points1;
            vector<Point2f> points2;
            for (const auto& match: matches) {
                int i1 = match.queryIdx;
                int i2 = match.trainIdx;
                points1.push_back(k1[i1].pt);
                points2.push_back(k2[i2].pt);
                //cout << k1[i1].pt << k2[i2].pt << endl;
            }
            Mat F = findFundamentalMat(points1, points2, FM_RANSAC, 3, 0.99);
            Mat P1;
            Mat P2;
           projectionsFromFundamental(F, P1, P2);
            Mat camera;
            Mat rot;
            Mat trans;
            decomposeProjectionMatrix(P2, camera, rot, trans);
            trans = trans / trans.at<double>(3);
            Mat trans1 = (Mat_<double>(3,1) << trans.at<double>(0), trans.at<double>(1), trans.at<double>(2));
            Mat t;
            hconcat(rot, trans1, t);
            Mat temp = (Mat_<double>(1,4) << 0,0,0,1);
            vconcat(t, temp, t);
            pos = t * pos;
            cout << pos << endl;
        }
        k1 = k2;
        d1 = d2;

        /*
        // Harris detector
        Mat output, output_scaled;
        corner::cornerHarris_detect(frame,gray,output);
        //convertScaleAbs(output, output_scaled);
        /// Drawing a circle around corners
        output_scaled = gray;
        bool isSuccess = corner::detectHarrisKeyPoints(output, output_scaled);
        if(isSuccess) {
          imshow("Harris detector", output_scaled);
        } else {
          cout <<"Skipped showing frame: "<<frameNumber;
        }
        */



        // Calls Shi-Tomas detector.
        Mat shiTomasOutput, shiTomasAndSiftOutput;
        corner::shiTomasiDetector(gray, shiTomasOutput, 200);
        keypoints.clear();
        cv::drawKeypoints(shiTomasOutput, keypoints, shiTomasAndSiftOutput);
        imshow("Shi-Tomas detectors", shiTomasAndSiftOutput);


        cout<<"Processed frame: "<<frameNumber<<endl;
        frameNumber++;
        cv::drawKeypoints(gray, k2, output);
        //imshow("gray", output);
        if(waitKey(30) >= 0) {
            break;
        }
    }

    waitKey();
    return 0;
}
