// Copyright (C) 2014 The Regents of the University of California (Regents).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of The Regents or University of California nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// Author: Chris Sweeney (cmsweeney@cs.ucsb.edu)

#include <Eigen/Core>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <theia/theia.h>
#include <string>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#ifdef FREEGLUT
#include <GL/freeglut.h>
#else  // FREEGLUT
#include <GLUT/glut.h>
#endif  // FREEGLUT
#else  // __APPLE__
#ifdef _WIN32
#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#else  // _WIN32
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif  // _WIN32
#endif  // __APPLE__

DEFINE_string(reconstruction, "", "Reconstruction file to be viewed.");

#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include "theia/sfm/types.h"


#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
//#include <pcl/search/flann_search.h>
//#include <pcl/kdtree/flann.h>

#include "libelas/src/elas.h"
#include "libelas/src/image.h"
#include "libelas/src/linterp.h"

#include "opencv2/viz/types.hpp"

#include "opencv2/ximgproc/disparity_filter.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/xphoto.hpp"

#include "obj_loader.h"
#include "ImageSegmentation.h"

using namespace std;
using namespace cv;
using namespace cv::sfm;
using namespace cv::viz;
using namespace theia;
using namespace pcl;
// Containers for the data.
std::vector<theia::Camera> cameras;
std::vector<Eigen::Vector3d> world_points;
std::vector<Eigen::Vector3f> point_colors;
std::vector<int> num_views_for_track;

struct Params {
  Mat K1;
  Mat R1;
  Mat T1;
  Mat M1;
  Mat d1;
  Mat K2;
  Mat R2;
  Mat T2;
  Mat M2;
  Mat d2;
  Mat Ra1;
  Mat Ra2;
  Mat P1;
  Mat P2;
  Mat Q;
  Rect roi1;
  Rect roi2;
  double baseline;
  Size im1size;
  Size im2size;
};

struct PointsView {
  Mat xyzw;
  Mat im1r;
  Params c;
  int i1;
  int i2;
  int diff;
};
bool sortF(PointsView i, PointsView j) { return (i.diff<j.diff); }
double round(double d) {
  return floor(d + 0.5);
}

void computeRectification(
    const Mat& im1,
    const Mat& im2,
    const View& view1,
    const View& view2,
    Params& c) {

  c.im1size = im1.size();
  c.im2size = im2.size();

  const auto& camera1 = view1.Camera();
  Eigen::Matrix3d eR1 = camera1.GetOrientationAsRotationMatrix();
  Eigen::Vector3d eT1 = camera1.GetPosition();
  Eigen::Matrix3d eK1;
  camera1.GetCalibrationMatrix(&eK1);
  Matrix3x4d eM1;
  camera1.GetProjectionMatrix(&eM1);
  double rd11 = camera1.RadialDistortion1();
  double rd12 = camera1.RadialDistortion2();

  const auto& camera2 = view2.Camera();
  Eigen::Matrix3d eR2 = camera2.GetOrientationAsRotationMatrix();
  Eigen::Vector3d eT2 = camera2.GetPosition();
  Eigen::Matrix3d eK2;
  camera2.GetCalibrationMatrix(&eK2);
  Matrix3x4d eM2;
  camera2.GetProjectionMatrix(&eM2);
  double rd21 = camera1.RadialDistortion1();
  double rd22 = camera1.RadialDistortion2();

  eigen2cv(eK1, c.K1);
  eigen2cv(eR1, c.R1);
  Mat T1temp;
  eigen2cv(eT1, T1temp);
  c.T1 = - c.R1 * T1temp;
  eigen2cv(eM1, c.M1);

  eigen2cv(eK2, c.K2);
  eigen2cv(eR2, c.R2);
  Mat T2temp;
  eigen2cv(eT2, T2temp);
  c.T2 = - c.R2 * T2temp;
  eigen2cv(eM2, c.M2);

  c.d1 = Mat::zeros(4,1, CV_64F);
  c.d1.at<double>(0,0) = rd11;
  c.d1.at<double>(1,0) = rd12;
  c.d2 = Mat::zeros(4,1, CV_64F);
  c.d2.at<double>(0,0) = rd21;
  c.d2.at<double>(1,0) = rd22;
  Mat R = c.R2 * c.R1.t();
  Mat T = c.T2 - c.R2 * c.R1.t() * c.T1;

  stereoRectify(c.K1, c.d1, c.K2, c.d2, im1.size(), R, T, c.Ra1, c.Ra2, c.P1, c.P2, c.Q, CALIB_ZERO_DISPARITY, -1, im1.size(), &c.roi1, &c.roi2);

  Mat translation = c.P2.colRange(0,3).inv() * c.P2.colRange(3,4);
  c.baseline = translation.at<double>(0,0);
}

bool areParamsAcceptable(const Params& c) {
  // Short circuit on bad baselines.
  if (fabs(c.baseline) < 1.0 || fabs(c.baseline) > 2.0) {
    return false;
  }

  if (c.P2.at<double>(1,3) != 0 || c.P2.at<double>(0,3) == 0) {
    // We can't deal with vertical rectification yet.
    return false;
  } 

  // Too much warping
  float w = c.im1size.width;
  float h = c.im1size.height;
  float w1 = c.roi1.width;
  float h1 = c.roi1.height;
  float w2 = c.roi2.width;
  float h2 = c.roi2.height;
  float ratio = (10.0 / 16.0);
  float wthresh = w * ratio;
  float hthresh = h * ratio;
  if (w1 < wthresh || h1 < hthresh || w2 < wthresh || h2 < hthresh) {
    return false;
  }

  return true; 
}


bool getDisparityMaps(const Mat& img1r, const Mat& img2r, Mat& dispf) {

  enum { STEREO_BM=0, STEREO_SGBM=1, STEREO_HH=2, STEREO_VAR=3, STEREO_3WAY=4 };
  int alg = STEREO_HH;

  Ptr<StereoSGBM> sgbm = StereoSGBM::create(0,16,3);
  int numberOfDisparities = ((img1r.size().width/8) + 15) & -16;

  sgbm->setPreFilterCap(63);
  int sgbmWinSize = 3;
  sgbm->setBlockSize(sgbmWinSize);
  int cn = img1r.channels();
  sgbm->setP1(8*cn*sgbmWinSize*sgbmWinSize);
  sgbm->setP2(32*cn*sgbmWinSize*sgbmWinSize);
  sgbm->setMinDisparity(0);
  sgbm->setNumDisparities(numberOfDisparities);
  sgbm->setUniquenessRatio(10);
  sgbm->setSpeckleWindowSize(100);
  sgbm->setSpeckleRange(32);
  sgbm->setDisp12MaxDiff(1);
  if(alg==STEREO_HH)
      sgbm->setMode(StereoSGBM::MODE_HH);
  else if(alg==STEREO_SGBM)
      sgbm->setMode(StereoSGBM::MODE_SGBM);
  else if(alg==STEREO_3WAY)
      sgbm->setMode(StereoSGBM::MODE_SGBM_3WAY);

  Mat disp, disp8;
  sgbm->compute(img1r, img2r, disp);
  if( alg != STEREO_VAR ) {
    disp.convertTo(disp8, CV_8U, 255/(numberOfDisparities*16.));
  } else {
    disp.convertTo(disp8, CV_8U);
  }
  //Mat dispf;
  disp.convertTo(dispf, CV_32F, 1.0 / 16.0);
  return true;
}


//bool getDepthMapsForRectifiedImages(const Mat& im1, const Mat& im2, const View& view1, const View& view2, Mat& xyz, Mat& xyzw, Mat& depth1, Mat& depth2, Mat& im1r, Mat& im2r, std::unique_ptr<theia::Reconstruction>& reconstruction, Params& c) {
bool getDepthMapsForRectifiedImages(const Mat& img1r, const Mat& img2r, Mat& xyz, Mat& xyzw, Mat& dispf, Params& c) {

  View view1;
  computeRectification(img1r, img1r, view1, view1, c);
  //cout << "K value" << c.K1 << endl;
  //c.K1 = Mat()
  //Mat Kinternal(3, 3, DataType<int>::type);
  //Kinternal(1,1) = 
  Mat Kinternal = (Mat_<double>(3,3) << 1200, 0, 720, 0, 1200, 340, 0, 0, 1);
  cout << Kinternal << endl;
  c.K1 = Kinternal;
 
  Mat disp, disp8;
  //Mat dispf;

  using namespace cv::ximgproc;

  Mat left = img1r.clone(), right = img2r.clone();
  Mat left_for_matcher = img1r.clone(), right_for_matcher = img2r.clone();
  Mat left_disp,right_disp;
  Mat filtered_disp;

  int numberOfDisparities = ((img1r.size().width/8) + 15) & -16;

  int wsize = 3;
  int max_disp = 160;
  //double vis_mult = 1.0;

  Ptr<StereoSGBM> left_matcher  = StereoSGBM::create(0,max_disp,wsize);
  left_matcher->setP1(24*wsize*wsize);
  left_matcher->setP2(96*wsize*wsize);
  left_matcher->setPreFilterCap(63);
  left_matcher->setMode(StereoSGBM::MODE_SGBM_3WAY);
  auto wls_filter = createDisparityWLSFilter(left_matcher);
  Ptr<StereoMatcher> right_matcher = createRightMatcher(left_matcher);

  cvtColor(left_for_matcher,  left_for_matcher,  COLOR_BGR2GRAY);
  cvtColor(right_for_matcher, right_for_matcher, COLOR_BGR2GRAY);
  left_matcher->compute(left_for_matcher, right_for_matcher, left_disp);
  right_matcher->compute(right_for_matcher, left_for_matcher, right_disp);
  //left_disp = left_disp/250;
  //right_disp = right_disp/250;
  //cv::imshow("left_depth", left_disp);
  //cv::imshow("right_depth", right_disp);
  //! [filtering]
  wls_filter->setLambda(8000.0);
  wls_filter->setSigmaColor(1.5);
  wls_filter->filter(left_disp, left, filtered_disp, right_disp);
  filtered_disp = filtered_disp / 250;
  cv::imshow("filtered_disp", filtered_disp);

  filtered_disp.convertTo(disp8, CV_8U, 255/(numberOfDisparities*16.));
  filtered_disp.convertTo(dispf, CV_32F, 1.0 / 16.0);

  return true;
}

int main(int argc, char* argv[]) {
  THEIA_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  std::srand ( unsigned ( std::time(0) ) );
  
  
  Mat im1r = imread(string("/home/andreib/Documents/cs231a/StereoImage/tsukuba/input/scene1.row3.col3.ppm"));
  Mat im2r = imread(string("/home/andreib/Documents/cs231a/StereoImage/tsukuba/input/scene1.row3.col4.ppm"));
  //Mat im1r;
  //Mat im2r; 
  //cvtColor(im1rcolor, im1r , CV_RGB2GRAY);
  //cvtColor(im2rcolor,  im2r,  CV_RGB2GRAY);
  Mat groundtruthcolor = imread(string("/home/andreib/Documents/cs231a/StereoImage/venus/groundtruth.pgm"));
  //Mat groundtruth;
  //cvtColor(groundtruthcolor,  groundtruth,  COLOR_BGR2GRAY);
  Mat xyz, xyzw, dispf;
  Params c;
  //bool ret = getDepthMapsForRectifiedImages(im1r, im2r, xyz, xyzw, dispf, c);
  bool ret = getDisparityMaps(im1r, im2r, dispf);
  if (!ret) {
    cout << "Depths not found";
  }
  
  Mat Kinternal = (Mat_<double>(3,3) << 1200, 0, 720, 0, 1200, 340, 0, 0, 1);
  Mat smoothed = smooth_depth_map_using_segmentation(im1r, dispf, Kinternal);
  dispf = dispf * 10.0;
  smoothed = smoothed * 10.0;
  cv::imshow("depth", dispf);
  cv::imshow("smoothed", smoothed);
  cv::imshow("orig", im1r);
 // double minValue, maxValue; 
 // Point minPoint, maxPoint;
 // minMaxLoc(groundtruthcolor,&minValue, &maxValue, &minPoint, &maxPoint);
 // cout<<"minMax of groundtruth: "<<minValue <<" "<<maxValue<<" "<<minPoint<<" "<<maxPoint<<endl;

  cout <<"shape: "<<im1r.size() << endl;
  cout << "groundtruth shape: "<<groundtruthcolor.size() << endl;

  double minValue1, maxValue1; 
  Point minPoint1, maxPoint1;
  minMaxLoc(dispf,&minValue1, &maxValue1, &minPoint1, &maxPoint1);
  cout<<"minMax of dispf: "<<minValue1 <<" "<<maxValue1<<" "<<minPoint1<<" "<<maxPoint1<<endl;
  imwrite("tsukuba-dispf.jpg", dispf);
  imwrite("tsukuba_smoothed_disp.jpg", smoothed);
  waitKey();
  return 0;
}
