#include <string>
#include <opencv2/core.hpp>
#include <Eigen/Core>

using namespace cv;
using namespace std;

class ObjLoader {
  public:
	ObjLoader(string filename, double scale);

	void setXYZW();
	void makeSmallerTriangles();

	vector<Eigen::Vector4d> vertices;
	Mat xyzw;
	vector<Vec3i> triangles;
};