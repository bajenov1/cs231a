// Copyright (C) 2014 The Regents of the University of California (Regents).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of The Regents or University of California nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// Author: Chris Sweeney (cmsweeney@cs.ucsb.edu)

#include <Eigen/Core>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <theia/theia.h>
#include <string>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#ifdef FREEGLUT
#include <GL/freeglut.h>
#else  // FREEGLUT
#include <GLUT/glut.h>
#endif  // FREEGLUT
#else  // __APPLE__
#ifdef _WIN32
#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#else  // _WIN32
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif  // _WIN32
#endif  // __APPLE__

DEFINE_string(reconstruction, "", "Reconstruction file to be viewed.");

#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include "theia/sfm/types.h"


#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#include "libelas/src/elas.h"
#include "libelas/src/image.h"

#include "opencv2/viz/types.hpp"

using namespace std;
using namespace cv;
using namespace cv::sfm;
using namespace cv::viz;
using namespace theia;
using namespace pcl;
// Containers for the data.
std::vector<theia::Camera> cameras;
std::vector<Eigen::Vector3d> world_points;
std::vector<Eigen::Vector3f> point_colors;
std::vector<int> num_views_for_track;

void saveXYZ(const char* filename, const Mat& mat)
{
    const double max_z = 1.0e4;
    FILE* fp = fopen(filename, "wt");
    for(int y = 0; y < mat.rows; y++)
    {
        for(int x = 0; x < mat.cols; x++)
        {
            Vec3f point = mat.at<Vec3f>(y, x);
            if(fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z) continue;
            fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
        }
    }
    fclose(fp);
}

Mesh loadModel(string filename) {
  return Mesh::load(filename, /* LOAD_OBJ */ 2);
}

int main(int argc, char* argv[]) {
  THEIA_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  auto model = loadModel("../../models/m1/144.obj");

  // Output as a binary file.
  std::unique_ptr<theia::Reconstruction> reconstruction(
      new theia::Reconstruction());
  CHECK(ReadReconstruction(FLAGS_reconstruction, reconstruction.get()))
      << "Could not read reconstruction file.";

  // Centers the reconstruction based on the absolute deviation of 3D points.
  reconstruction->Normalize();

  map<string, Mat> images;
  map<string, Eigen::Matrix3d> Rs;
  map<string, Eigen::Vector3d> Ts;
  map<string, Eigen::Matrix3d> Ks;
  map<string, Matrix3x4d> Ms;
  map<string, View> Vs;
  double rd1 = 0;
  double rd2 = 0;
  // Set up camera drawing.
  cameras.reserve(reconstruction->NumViews());
  for (const theia::ViewId view_id : reconstruction->ViewIds()) {
    const auto* view = reconstruction->View(view_id);
    if (view == nullptr || !view->IsEstimated()) {
      continue;
    }
    Mat im = imread(string("/home/andreib/cs231/cs231a/media/sequence2/") + view->Name());
    theia::Matrix3x4d proj;
    view->Camera().GetProjectionMatrix(&proj);
    cameras.emplace_back(view->Camera());

    Eigen::Vector2d pt;
    view->Camera().ProjectPoint(Eigen::Vector4d(0,0,0,1), &pt);
    // circle(im, Point(pt(0), pt(1)), 1, Scalar( 255, 0, 0), 50);
    images[view->Name()] = im;


    //////
    const auto& camera = view->Camera();
    Eigen::Matrix3d R = camera.GetOrientationAsRotationMatrix();
    Eigen::Vector3d T = camera.GetPosition();
    Eigen::Matrix3d K;
    camera.GetCalibrationMatrix(&K);
    Matrix3x4d M;
    camera.GetProjectionMatrix(&M);
    Rs[view->Name()] = R;
    Ts[view->Name()] = T;
    Ks[view->Name()] = K;
    Ms[view->Name()] = M;
    rd1 = camera.RadialDistortion1();
    rd2 = camera.RadialDistortion2();
    Vs[view->Name()] = *view;
    /////
  }

  string name1 = string("seq") + to_string(10) + ".jpg";
  string name2 = string("seq") + to_string(0) + ".jpg";
  auto eK1 = Ks[name1];
  Mat K1;
  eigen2cv(eK1, K1);
  auto eR1 = Rs[name1];
  Mat R1;
  eigen2cv(eR1, R1);
  Mat R1temp;
  transpose(R1, R1temp);

  auto eT1 = Ts[name1];
  Mat T1temp;
  eigen2cv(eT1, T1temp);
  Mat T1 = - R1 * T1temp;

  auto eM1 = Ms[name1];
  Mat M1;
  eigen2cv(eM1, M1);

  auto eK2 = Ks[name2];
  Mat K2;
  eigen2cv(eK2, K2);
  auto eR2 = Rs[name2];
  Mat R2;
  eigen2cv(eR2, R2);
  Mat R2temp;
  transpose(R2, R2temp);


  auto eT2 = Ts[name2];
  Mat T2temp;
  eigen2cv(eT2, T2temp);
  Mat T2 = - R2 * T2temp;

  auto eM2 = Ms[name2];
  Mat M2;
  eigen2cv(eM2, M2);

  Mat Ra1;
  Mat Ra2;
  Mat P1;
  Mat P2;
  Mat Q;
  Mat d1 = Mat::zeros(4,1, CV_64F);
  d1.at<double>(0,0) = rd1;
  d1.at<double>(1,0) = rd2;
  Mat d2 = d1;
  Mat R1t;
  transpose(R1, R1t);
  Mat R = R2 * R1t;
  Mat T = T2 - R2 * R1t * T1;
  Mat im1 = images[name1];
  Mat im2 = images[name2];

  Rect roi1, roi2;
  stereoRectify(K1, d1, K2, d2, im1.size(), R, T, Ra1, Ra2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, im1.size(), &roi1, &roi2);

  Mat map11, map12, map21, map22;
  initUndistortRectifyMap(K1, d1, Ra1, P1, im1.size(), CV_32FC1, map11, map12);
  initUndistortRectifyMap(K2, d2, Ra2, P2, im1.size(), CV_32FC1, map21, map22);

  Mat img1r, img2r;
  remap(im1, img1r, map11, map12, INTER_LINEAR);
  remap(im2, img2r, map21, map22, INTER_LINEAR);
  

  /// testing
  cv::imshow("orig", im1);
  cv::imshow("rectified", img1r);
  Mat Ra1t;
  transpose(Ra1, Ra1t);
  Mat Ra2t;
  transpose(Ra2, Ra2t);
  Mat m1;
  Mat m2;
  Mat Ktrans = P1(cv::Rect(0,0,3,3));
  initUndistortRectifyMap(Ktrans, d1, Ra1t, K1, im1.size(), CV_32FC1, m1, m2);
  Mat imtest;
  remap(img1r, imtest, m1, m2, INTER_LINEAR);
  cv::imshow("back orig", imtest);
  //waitKey();
  /// testing

  im1 = img1r;
  im2 = img2r;


  ////
  const auto& view = Vs[name1];
  const auto& tracks = view.TrackIds();
  for (const auto& id : tracks) {
    const Feature* f = view.GetFeature(id);
    Mat mp;
    eigen2cv(*f, mp);
    Mat mpf(1,1,CV_64FC2);
    mpf.at<cv::Vec2d>(0) = Vec2d(mp.at<double>(0), mp.at<double>(1));
    //cout << mpf << endl;
    Mat rmp;
    undistortPoints(mpf, rmp, K1, d1, Ra1, P1);
    Point p;
    p.x = rmp.at<double>(0);
    p.y = rmp.at<double>(1);
    drawMarker(im1, p, Scalar( 255, 0, 0));
  }
  ////

  ///////////////////////  ELAS //////
  Mat eim1;
  Mat eim2;
  cv::cvtColor(im1, eim1, CV_BGR2GRAY);
  cv::cvtColor(im2, eim2, CV_BGR2GRAY);

  //cv::imshow("1", eim1);
  //cv::imshow("2", eim2);

  const int32_t dims[3] = {im1.cols,im1.rows,im1.cols};
  Mat dim1(im1.rows, im1.cols, CV_32FC1);
  Mat dim2(im2.rows, im2.cols, CV_32FC1);

  Elas::parameters param;
  param.postprocess_only_left = false;
  Elas elas(param);
  elas.process(eim1.data, eim2.data, (float *) dim1.data, (float *) dim2.data, dims);

  double min, max;
  cv::minMaxLoc(dim1, &min, &max);
  dim1 = dim1 / max;
  cv::minMaxLoc(dim2, &min, &max);
  dim2 = dim2 / max;

  //cv::imshow("dim1", dim1);
  //cv::imshow("dim2", dim2);
  /////////////////////////////////


  enum { STEREO_BM=0, STEREO_SGBM=1, STEREO_HH=2, STEREO_VAR=3, STEREO_3WAY=4 };
  int alg = STEREO_HH;

  Ptr<StereoSGBM> sgbm = StereoSGBM::create(0,16,3);
  int numberOfDisparities = ((im1.size().width/8) + 15) & -16;

  sgbm->setPreFilterCap(63);
  int sgbmWinSize = 3;
  sgbm->setBlockSize(sgbmWinSize);
  int cn = im1.channels();
  sgbm->setP1(8*cn*sgbmWinSize*sgbmWinSize);
  sgbm->setP2(32*cn*sgbmWinSize*sgbmWinSize);
  sgbm->setMinDisparity(0);
  sgbm->setNumDisparities(numberOfDisparities);
  sgbm->setUniquenessRatio(10);
  sgbm->setSpeckleWindowSize(100);
  sgbm->setSpeckleRange(32);
  sgbm->setDisp12MaxDiff(1);
  if(alg==STEREO_HH)
      sgbm->setMode(StereoSGBM::MODE_HH);
  else if(alg==STEREO_SGBM)
      sgbm->setMode(StereoSGBM::MODE_SGBM);
  else if(alg==STEREO_3WAY)
      sgbm->setMode(StereoSGBM::MODE_SGBM_3WAY);


  Mat disp, disp8;
  sgbm->compute(im1, im2, disp);
  if( alg != STEREO_VAR ) {
    disp.convertTo(disp8, CV_8U, 255/(numberOfDisparities*16.));
  } else {
    disp.convertTo(disp8, CV_8U);
  }
  //cout << disp << endl;
  Mat dispf;
  disp.convertTo(dispf, CV_32F, 1.0);

  Mat xyz;
  reprojectImageTo3D(dispf, xyz, Q, true);
  Mat points(1,xyz.rows * xyz.cols, CV_32FC3);
  int idx = 0;
  cout << "Ra1" << Ra1 << endl;

  for(int y = 0; y < xyz.rows; y++) {
    for(int x = 0; x < xyz.cols; x++) {
      Vec3f point = xyz.at<Vec3f>(y, x);
      Mat pointM(point);
      Mat pointM64;
      Mat TM1(T1);
      pointM.convertTo(pointM64, CV_64F);
      Mat rotpt = R1t * Ra1t * pointM64 -  R1t * TM1;
      //cout << pointM64 << rotpt << endl;
      Vec3f rotptv(rotpt);
      points.at<Vec3f>(idx) = rotptv;
      idx++;
    }
  }
  
  cout << "print warped" << endl;
  cout << Ra1 << endl;
  cout << P1 << endl;
  Mat out;
  cv::projectPoints(points, /*Mat::eye(3,3,CV_64F), Vec3f(0,0,0), P1.colRange(0,3),*/ R1, T1, K1, d1, out);
  //cout << out << endl;
  idx = 0;
  Mat imout(im1.size(), CV_8UC3);
  imout.setTo(Scalar(0,0,0));
  cout << "blah" << endl;
  cout << im1.size() << im1.type() << endl;
  cout << imout.size() << imout.type() << endl;
  cout << out.size() << out.type() << endl;
  for(int y = 0; y < xyz.rows; y++) {
    for(int x = 0; x < xyz.cols; x++) {
      Vec3b color = im1.at<Vec3b>(y, x);
      const Vec2f& p = out.at<Vec2f>(idx);
      //cout << idx << " " << p[0] << " " << p[1] << endl;
      if (p[1] != p[1] || p[0] != p[0] || p[1] < 0 || p[0] < 0 || p[1] >= im1.rows || p[0] >= im1.cols) {
        idx++;
        continue;
      }
      imout.at<Vec3b>(p[1], p[0]) = color;
      idx++;
    }
  }
  cv::imshow("hope", imout);

  int N = dispf.rows * dispf.cols;
  Mat p1(1, N, CV_64FC2);
  Mat p2(1, N, CV_64FC2);
  idx = 0;
  Mat imt(im1.size(), CV_8UC3);
  imt.setTo(Scalar(0,0,0));
  cout << dispf.size() << " " << dispf.rows << " " << dispf.cols << endl;
  for(int y = 0; y < dispf.rows; y++) {
    for(int x = 0; x < dispf.cols; x++) {
      float disp = dispf.at<float>(y, x);
      if (disp < 0) {
        continue;
      }
      p1.at<Vec2f>(idx) = Vec2f(y, x);
      p2.at<Vec2f>(idx) = Vec2f(y, x - disp / 16.0);
      imt.at<Vec3b>(y, x - disp / 16.0) = im1.at<Vec3b>(y, x);
      //cout << p1.at<Vec2d>(idx) << p2.at<Vec2d>(idx) << endl;
      idx++;
    }
  }
  //cv::imshow("imt", imt);
  //cout << idx << endl;
  cout << "blah: " << p1.colRange(0,idx).size() << endl;
  Mat p1u;
  Mat p2u;
  undistortPoints(p1.colRange(0,idx), p1u, P1.colRange(0,3), d1, Ra1t, K1);
  undistortPoints(p2.colRange(0,idx), p2u, P2.colRange(0,3), d2, Ra2t, K2);
  //p1u = p1.colRange(0,idx);
  //p2u = p2.colRange(0,idx);
  cout << P1 << " " << P1.type() << endl;
  //cout << p1u << endl;
  cv::Mat world(1,N,CV_64FC4);
  Mat a1(3,4,CV_64F);
  Mat a2(3,4,CV_64F);
  a1.at<double>(0,0) = R1.at<double>(0,0);
  a1.at<double>(0,1) = R1.at<double>(0,1);
  a1.at<double>(0,2) = R1.at<double>(0,2);
  a1.at<double>(0,3) = T1.at<double>(0);
  a1.at<double>(1,0) = R1.at<double>(1,0);
  a1.at<double>(1,1) = R1.at<double>(1,1);
  a1.at<double>(1,2) = R1.at<double>(1,2);
  a1.at<double>(1,3) = T1.at<double>(1);
  a1.at<double>(2,0) = R1.at<double>(2,0);
  a1.at<double>(2,1) = R1.at<double>(2,1);
  a1.at<double>(2,2) = R1.at<double>(2,2);
  a1.at<double>(2,3) = T1.at<double>(2);
  cout << "a1: " << a1 << endl;
  cout << R1 << endl;
  cout << T1 << endl;
  a2.at<double>(0,0) = R2.at<double>(0,0);
  a2.at<double>(0,1) = R2.at<double>(0,1);
  a2.at<double>(0,2) = R2.at<double>(0,2);
  a2.at<double>(0,3) = T2.at<double>(0);
  a2.at<double>(1,0) = R2.at<double>(1,0);
  a2.at<double>(1,1) = R2.at<double>(1,1);
  a2.at<double>(1,2) = R2.at<double>(1,2);
  a2.at<double>(1,3) = T2.at<double>(1);
  a2.at<double>(2,0) = R2.at<double>(2,0);
  a2.at<double>(2,1) = R2.at<double>(2,1);
  a2.at<double>(2,2) = R2.at<double>(2,2);
  a2.at<double>(2,3) = T2.at<double>(2);
  cv::triangulatePoints(a1, a2, p1u, p2u, world);
  //cout << p1u << endl;
  cout << world.type() << " " <<  world.size() << " " << world.rows << " " << world.cols << endl;
  //cout << world << endl;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  const double max_z = 10.0;
  for(int y = 0; y < xyz.rows; y++)
  {
      for(int x = 0; x < xyz.cols; x++)
      {
          Vec3f point = xyz.at<Vec3f>(y, x);
          Vec3b color = im1.at<Vec3b>(y, x);
          PointXYZRGB pt(color[2], color[1], color[0]);
          pt.x = point[0];
          pt.y = point[1];
          pt.z = point[2];
          if(fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z) continue;
          //cout << pt << endl;
          cloud->push_back(pt);
      }
  }
  /*pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  for(int x = 0; x < world.cols; x++)
  {
    const auto& pt = world.at<Vec4d>(x);
    PointXYZ mp;
    mp.x = pt[0] / pt[3];
    mp.y = pt[1] / pt[3];
    mp.z = pt[2] / pt[3];
    //cout << mp << endl;
    cloud->push_back(mp);
  }*/

  /*namedWindow("left", 1);
  cv::imshow("left", im1);
  namedWindow("right", 1);
  cv::imshow("right", im2);
  namedWindow("disparity", 0);
  cv::imshow("disparity", disp8);*/
  //printf("press any key to continue...");
  //fflush(stdout);
  //waitKey();
  //printf("\n");
  waitKey();
  pcl::visualization::CloudViewer viewer("Simple Cloud Viewer");
  viewer.showCloud(cloud);
  while (!viewer.wasStopped()) {
    sleep(10);
  }

  reconstruction.release();
  return 0;
}
