// Copyright (C) 2014 The Regents of the University of California (Regents).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of The Regents or University of California nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// Author: Chris Sweeney (cmsweeney@cs.ucsb.edu)

#include <Eigen/Core>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <theia/theia.h>
#include <string>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#ifdef FREEGLUT
#include <GL/freeglut.h>
#else  // FREEGLUT
#include <GLUT/glut.h>
#endif  // FREEGLUT
#else  // __APPLE__
#ifdef _WIN32
#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#else  // _WIN32
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif  // _WIN32
#endif  // __APPLE__

DEFINE_string(reconstruction, "", "Reconstruction file to be viewed.");

#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include "theia/sfm/types.h"


#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
//#include <pcl/search/flann_search.h>
//#include <pcl/kdtree/flann.h>

#include "libelas/src/elas.h"
#include "libelas/src/image.h"
#include "libelas/src/linterp.h"

#include "opencv2/viz/types.hpp"

#include "opencv2/ximgproc/disparity_filter.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/xphoto.hpp"

#include "obj_loader.h"
#include "ImageSegmentation.h"

using namespace std;
using namespace cv;
using namespace cv::sfm;
using namespace cv::viz;
using namespace theia;
using namespace pcl;
// Containers for the data.
std::vector<theia::Camera> cameras;
std::vector<Eigen::Vector3d> world_points;
std::vector<Eigen::Vector3f> point_colors;
std::vector<int> num_views_for_track;

struct Params {
  Mat K1;
  Mat R1;
  Mat T1;
  Mat M1;
  Mat d1;
  Mat K2;
  Mat R2;
  Mat T2;
  Mat M2;
  Mat d2;
  Mat Ra1;
  Mat Ra2;
  Mat P1;
  Mat P2;
  Mat Q;
  Rect roi1;
  Rect roi2;
  double baseline;
  Size im1size;
  Size im2size;
};

struct PointsView {
  Mat xyzw;
  Mat im1r;
  Params c;
  int i1;
  int i2;
  int diff;
};
bool sortF(PointsView i, PointsView j) { return (i.diff<j.diff); }
double round(double d) {
  return floor(d + 0.5);
}

void computeRectification(
    const Mat& im1,
    const Mat& im2,
    const View& view1,
    const View& view2,
    Params& c) {

  c.im1size = im1.size();
  c.im2size = im2.size();

  const auto& camera1 = view1.Camera();
  Eigen::Matrix3d eR1 = camera1.GetOrientationAsRotationMatrix();
  Eigen::Vector3d eT1 = camera1.GetPosition();
  Eigen::Matrix3d eK1;
  camera1.GetCalibrationMatrix(&eK1);
  Matrix3x4d eM1;
  camera1.GetProjectionMatrix(&eM1);
  double rd11 = camera1.RadialDistortion1();
  double rd12 = camera1.RadialDistortion2();

  const auto& camera2 = view2.Camera();
  Eigen::Matrix3d eR2 = camera2.GetOrientationAsRotationMatrix();
  Eigen::Vector3d eT2 = camera2.GetPosition();
  Eigen::Matrix3d eK2;
  camera2.GetCalibrationMatrix(&eK2);
  Matrix3x4d eM2;
  camera2.GetProjectionMatrix(&eM2);
  double rd21 = camera1.RadialDistortion1();
  double rd22 = camera1.RadialDistortion2();

  eigen2cv(eK1, c.K1);
  eigen2cv(eR1, c.R1);
  Mat T1temp;
  eigen2cv(eT1, T1temp);
  c.T1 = - c.R1 * T1temp;
  eigen2cv(eM1, c.M1);

  eigen2cv(eK2, c.K2);
  eigen2cv(eR2, c.R2);
  Mat T2temp;
  eigen2cv(eT2, T2temp);
  c.T2 = - c.R2 * T2temp;
  eigen2cv(eM2, c.M2);

  c.d1 = Mat::zeros(4,1, CV_64F);
  c.d1.at<double>(0,0) = rd11;
  c.d1.at<double>(1,0) = rd12;
  c.d2 = Mat::zeros(4,1, CV_64F);
  c.d2.at<double>(0,0) = rd21;
  c.d2.at<double>(1,0) = rd22;
  Mat R = c.R2 * c.R1.t();
  Mat T = c.T2 - c.R2 * c.R1.t() * c.T1;

  stereoRectify(c.K1, c.d1, c.K2, c.d2, im1.size(), R, T, c.Ra1, c.Ra2, c.P1, c.P2, c.Q, CALIB_ZERO_DISPARITY, -1, im1.size(), &c.roi1, &c.roi2);

  Mat translation = c.P2.colRange(0,3).inv() * c.P2.colRange(3,4);
  c.baseline = translation.at<double>(0,0);
}

bool areParamsAcceptable(const Params& c) {
  // Short circuit on bad baselines.
  if (fabs(c.baseline) < 1.0 || fabs(c.baseline) > 2.0) {
    return false;
  }

  if (c.P2.at<double>(1,3) != 0 || c.P2.at<double>(0,3) == 0) {
    // We can't deal with vertical rectification yet.
    return false;
  } 

  // Too much warping
  float w = c.im1size.width;
  float h = c.im1size.height;
  float w1 = c.roi1.width;
  float h1 = c.roi1.height;
  float w2 = c.roi2.width;
  float h2 = c.roi2.height;
  float ratio = (10.0 / 16.0);
  float wthresh = w * ratio;
  float hthresh = h * ratio;
  if (w1 < wthresh || h1 < hthresh || w2 < wthresh || h2 < hthresh) {
    return false;
  }

  return true; 
}

bool getDepthMaps(const Mat& im1, const Mat& im2, const View& view1, const View& view2, Mat& xyz, Mat& xyzw, Mat& depth1, Mat& depth2, Mat& im1r, Mat& im2r, std::unique_ptr<theia::Reconstruction>& reconstruction, Params& c) {
  computeRectification(im1, im2, view1, view2, c);

  if (!areParamsAcceptable(c)) {
    return false;
  }

  if (c.P2.at<double>(0,3) > 0) {
    //cout << "flipped: " << c.P2 << endl;
    return getDepthMaps(im2, im1, view2, view1, xyz, xyzw, depth2, depth1, im2r, im1r, reconstruction, c);
  }

  Mat map11, map12, map21, map22;
  initUndistortRectifyMap(c.K1, c.d1, c.Ra1, c.P1, im1.size(), CV_32FC1, map11, map12);
  initUndistortRectifyMap(c.K2, c.d2, c.Ra2, c.P2, im1.size(), CV_32FC1, map21, map22);


  Mat img1r, img2r;
  remap(im1, img1r, map11, map12, INTER_LINEAR);
  remap(im2, img2r, map21, map22, INTER_LINEAR);

  // Output
  im1r = Mat(img1r);
  im2r = Mat(img2r);

  Mat disp, disp8;
  Mat dispf;

  using namespace cv::ximgproc;

  Mat left = img1r.clone(), right = img2r.clone();
  Mat left_for_matcher = img1r.clone(), right_for_matcher = img2r.clone();
  Mat left_disp,right_disp;
  Mat filtered_disp;

  int numberOfDisparities = ((img1r.size().width/8) + 15) & -16;

  int wsize = 3;
  int max_disp = 160;
  //double vis_mult = 1.0;

  Ptr<StereoSGBM> left_matcher  = StereoSGBM::create(0,max_disp,wsize);
  left_matcher->setP1(24*wsize*wsize);
  left_matcher->setP2(96*wsize*wsize);
  left_matcher->setPreFilterCap(63);
  left_matcher->setMode(StereoSGBM::MODE_SGBM_3WAY);
  auto wls_filter = createDisparityWLSFilter(left_matcher);
  Ptr<StereoMatcher> right_matcher = createRightMatcher(left_matcher);

  cvtColor(left_for_matcher,  left_for_matcher,  COLOR_BGR2GRAY);
  cvtColor(right_for_matcher, right_for_matcher, COLOR_BGR2GRAY);
  left_matcher->compute(left_for_matcher, right_for_matcher, left_disp);
  right_matcher->compute(right_for_matcher, left_for_matcher, right_disp);

  //! [filtering]
  wls_filter->setLambda(8000.0);
  wls_filter->setSigmaColor(1.5);
  wls_filter->filter(left_disp, left, filtered_disp, right_disp);

  filtered_disp.convertTo(disp8, CV_8U, 255/(numberOfDisparities*16.));
  filtered_disp.convertTo(dispf, CV_32F, 1.0 / 16.0);

  reprojectImageTo3D(dispf, xyz, c.Q, true);
  xyzw = Mat(1,xyz.rows * xyz.cols, CV_64FC3);
  int idx = 0;
  for(int y = 0; y < xyz.rows; y++) {
    for(int x = 0; x < xyz.cols; x++) {
      Vec3f point = xyz.at<Vec3f>(y, x);
      Mat pointM(point);
      Mat pointM64;
      Mat TM1(c.T1);
      pointM.convertTo(pointM64, CV_64F);
      Mat rotpt = c.R1.t() * c.Ra1.t() * pointM64 -  c.R1.t() * TM1;
      Vec3d rotptv(rotpt);
      xyzw.at<Vec3d>(idx) = rotptv;
      idx++;
    }
  }

  Mat out1;
  cv::projectPoints(xyzw, c.R1, c.T1, c.K1, c.d1, out1);
  Mat out2;
  cv::projectPoints(xyzw, c.R2, c.T2, c.K2, c.d2, out2);
  
  idx = 0;
  depth1 = Mat(im1.size(), CV_32F);
  depth1.setTo(0);
  for(int y = 0; y < xyz.rows; y++) {
    for(int x = 0; x < xyz.cols; x++) {
      const Vec3d& point = xyzw.at<Vec3d>(idx);
      const Vec2d& p = out1.at<Vec2d>(idx);
      int py = round(p[1]);
      int px = round(p[0]);
      if (p[1] != p[1] || p[0] != p[0] || py < 0 || px < 0 || py >= im1.rows || px >= im1.cols) {
        idx++;
        continue;
      }
      Mat pointM(point);
      Mat pointM64;
      Mat TM1(c.T1);
      pointM.convertTo(pointM64, CV_64F);
      Mat rotpt = c.R1 * pointM64 + TM1;
      Vec3f rotptv(rotpt);

      // Occlusions
      float cur = depth1.at<float>(py, px);
      if (cur == 0 || rotptv[2] < cur) {
        cur = rotptv[2];
      }
      depth1.at<float>(py, px) = cur;
      idx++;
    }
  }

  idx = 0;
  depth2 = Mat(im2.size(), CV_32F);
  depth2.setTo(0);
  for(int y = 0; y < xyz.rows; y++) {
    for(int x = 0; x < xyz.cols; x++) {
      const Vec3d& point = xyzw.at<Vec3d>(idx);
      const Vec2d& p = out2.at<Vec2d>(idx);
      int py = round(p[1]);
      int px = round(p[0]);
      if (p[1] != p[1] || p[0] != p[0] || py < 0 || px < 0 || py >= im2.rows || px >= im2.cols) {
        idx++;
        continue;
      }
      Mat pointM(point);
      Mat pointM64;
      Mat TM2(c.T2);
      pointM.convertTo(pointM64, CV_64F);
      Mat rotpt = c.R2 * pointM64 + TM2;
      Vec3f rotptv(rotpt);
      // Occlusions
      float cur = depth2.at<float>(py, px);
      if (cur == 0 || rotptv[2] < cur) {
        cur = rotptv[2];
      }
      depth2.at<float>(py, px) = cur;
      idx++;
    }
  }
  return true;
}

int main(int argc, char* argv[]) {
  THEIA_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  std::srand ( unsigned ( std::time(0) ) );
  // Output as a binary file.
  std::unique_ptr<theia::Reconstruction> reconstruction(
      new theia::Reconstruction());
  CHECK(ReadReconstruction(FLAGS_reconstruction, reconstruction.get()))
      << "Could not read reconstruction file.";

  // Centers the reconstruction based on the absolute deviation of 3D points.
  reconstruction->Normalize();

  map<string, Mat> images;
  map<string, View> Vs;
  map<string, theia::Camera> viewToCamera;
  // Set up camera drawing.
  cameras.reserve(reconstruction->NumViews());
  for (const theia::ViewId view_id : reconstruction->ViewIds()) {
    const auto* view = reconstruction->View(view_id);
    if (view == nullptr || !view->IsEstimated()) {
      continue;
    }
    Mat im = imread(string("/home/andreib/cs231/cs231a/media/sequence2/") + view->Name());
    theia::Matrix3x4d proj;
    view->Camera().GetProjectionMatrix(&proj);
    cameras.emplace_back(view->Camera());

    Eigen::Vector2d pt;
    view->Camera().ProjectPoint(Eigen::Vector4d(0,0,0,1), &pt);
    // circle(im, Point(pt(0), pt(1)), 1, Scalar( 255, 0, 0), 50);
    images[view->Name()] = im;
    Vs[view->Name()] = *view;
    viewToCamera[view->Name()] = view->Camera();
    /////
  }
  
  /*
  string namet = string("seq") + to_string(180) + ".jpg";
  Mat imt = images[namet];
  const View& viewt = Vs[namet];
  Params ct;
  computeRectification(imt, imt, viewt, viewt, ct);

  Mat im = imread(string("/home/andreib/cs231/cs231a/media/sequence2/seq180.jpg"));
  Mat dm;
  FileStorage fs("depth/depth_180.yml", FileStorage::READ );
  fs["depth"] >> dm;

  Mat smoothed = smooth_depth_map_using_segmentation(im, dm, ct.K1);
  dm = dm / 250.0;
  smoothed = smoothed / 250.0;
  cv::imshow("depth", dm);
  cv::imshow("smoothed", smoothed);
  cv::imshow("orig", im);
  waitKey();
  return 0;
  */

  vector<PointsView> allPoints;
  for (int j = 30; j < 420; j += 20) {
    string name1 = string("seq") + to_string(j) + ".jpg";
    for (int i = j - 30; i <= j + 30; i++) {
      string name2 = string("seq") + to_string(i) + ".jpg";
      Mat xyz, xyzw, depth1, depth2, im1r, im2r;
      const Mat& im1 = images[name1];
      const Mat& im2 = images[name2];
      const View& view1 = Vs[name1];
      const View& view2 = Vs[name2];
      Params c;
      computeRectification(im1, im2, view1, view2, c);
      if (!areParamsAcceptable(c)) {
        continue;
      }
      bool ret = getDepthMaps(im1, im2, view1, view2, xyz, xyzw, depth1, depth2, im1r, im2r, reconstruction, c);
      if (!ret) {
        continue;
      }
      PointsView view;
      view.xyzw = xyzw;
      view.im1r = im1r;
      view.c = c;
      view.i1 = j;
      view.i2 = i;
      allPoints.push_back(view);

      /*
      stringstream ss;
      ss << "pair/pair_" << j << "_" << i << ".yml";
      string filename = ss.str();
      FileStorage fs(filename, FileStorage::WRITE );
      fs << "xyzw" << xyzw;
      fs << "xyz" << xyz;
      fs << "im1r" << im1r;
      fs << "im2r" << im2r;
      fs << "depth1" << depth1;
      fs << "depth2" << depth2;
      fs << "j" << j;
      fs << "i" << i;
      */
      /*double min, max;
      cv::minMaxLoc(depth1, &min, &max);
      depth1 = depth1 / 250;
      cv::imshow("d1", depth1);
      cv::imshow("orig1", im1);
      waitKey();*/
      cout << j << " " << i << " " << c.baseline << " " << c.roi1 << " " << c.roi2 << " " << allPoints.size() << endl;
      // Advance faster
      i+=60;
    }
  }
    
  //ObjLoader obj("../../models/m2/Soccer Ball/Soccer Ball.obj", 0.05);
  ObjLoader obj("../../models/m1/144.obj", 1);
  obj.makeSmallerTriangles();
  obj.makeSmallerTriangles();

  for (int frame = 30; frame < 420; frame += 1) {
    cout << "Frame: " << frame << endl;
    string namet = string("seq") + to_string(frame) + ".jpg";
    Mat imt = images[namet];
    const View& viewt = Vs[namet];
    theia::Camera c65 = viewToCamera[namet];

    // Don't need to actually rectify, just using this to extract K,R,T from view.
    Params ct;
    computeRectification(imt, imt, viewt, viewt, ct);

    for (int i = 0; i < allPoints.size(); i++) {
      // Only use nearby frames;
      int i1 = allPoints[i].i1;
      allPoints[i].diff = abs(i1 - frame);
    }
    std::sort(allPoints.begin(), allPoints.end(), sortF);

    vector<Mat> viewDepths;
    int left = 0;
    int right = 0;
    for (int i = 0; i < allPoints.size(); i++) {
      // Only consider two views to left and right of cur frame
      if (left+right >= 4) {
        break;
      }
      if (left >= 3 || right >= 3) {
        continue;
      }

      int i1 = allPoints[i].i1;
      int diff = i1 - frame;
      if (diff > 0) {
        left++;
      } else {
        right++;
      }

      // Generate depth map for this view
      Mat viewdepth(imt.size(), CV_32F);
      viewdepth.setTo(0);
      const auto& xyzw = allPoints[i].xyzw;
      Mat out1;
      cv::projectPoints(xyzw, ct.R1, ct.T1, ct.K1, ct.d1, out1);
      
      int idx = 0;
      for(int y = 0; y < imt.rows; y++) {
        for(int x = 0; x < imt.cols; x++) {
          const Vec3d& point = xyzw.at<Vec3d>(idx);
          const Vec2d& p = out1.at<Vec2d>(idx);
          int py = round(p[1]);
          int px = round(p[0]);
          if (p[1] != p[1] || p[0] != p[0] || py < 0 || px < 0 || py >= imt.rows || px >= imt.cols) {
            idx++;
            continue;
          }
          Mat pointM(point);
          Mat pointM64;
          Mat TM1(ct.T1);
          pointM.convertTo(pointM64, CV_64F);
          Mat rotpt = ct.R1 * pointM64 + TM1;
          Vec3f rotptv(rotpt);
          // Occlusions
          float cur = viewdepth.at<float>(py, px);
          if (cur == 0 || rotptv[2] < cur) {
            cur = rotptv[2];
          }
          viewdepth.at<float>(py, px) = cur;
          idx++;
        }
      }
      viewDepths.push_back(viewdepth);
    }

    Mat deptht(imt.size(), CV_32F);
    deptht.setTo(0);
    // Average the accumulators
    for(int y = 0; y < deptht.rows; y++) {
      for(int x = 0; x < deptht.cols; x++) {
        vector<float> els;
        for (int i = 0; i < viewDepths.size(); i++) {
          float cur = viewDepths[i].at<float>(y,x);
          if (cur > 0) {
            els.push_back(cur);
          }
        }
        std::sort(els.begin(), els.end());
        if (els.size() == 1 || els.size() == 2) {
          deptht.at<float>(y, x) = els[0];
        } else if (els.size() > 2) {
          float diff = (els[1] - els[0]) / els[0];
          if (diff < 0.2) {
            deptht.at<float>(y, x) = els[0];
          } else {
            deptht.at<float>(y, x) = els[1];
          }
        }
      }
    }

    // Remove noise from depth map...
    //Mat temp; 
    //bilateralFilter(deptht, temp, 9, 75, 75);
    //deptht = temp;

    deptht = smooth_depth_map_using_segmentation(imt, deptht, ct.K1);

    Mat obj_out;
    cv::projectPoints(obj.xyzw, ct.R1, ct.T1, ct.K1, ct.d1, obj_out);

    vector<bool> obj_vertex_hidden(obj.vertices.size(), true);

    float da = 0;
    int dc = 0;
    for (int idx = 0; idx < obj.xyzw.cols; idx++) {
        const Vec3d& point = obj.xyzw.at<Vec3d>(idx);
        const Vec2d& p = obj_out.at<Vec2d>(idx);
        int py = round(p[1]);
        int px = round(p[0]);
        if (p[1] != p[1] || p[0] != p[0] || py < 0 || px < 0 || py >= imt.rows || px >= imt.cols) {
          continue;
        }
        Mat pointM(point);
        Mat pointM64;
        Mat TM1(ct.T1);
        pointM.convertTo(pointM64, CV_64F);
        Mat rotpt = ct.R1 * pointM64 + TM1;
        Vec3f rotptv(rotpt);
        auto my_depth = rotptv[2];
        // Occlusions
        float cur_depth = deptht.at<float>(py, px);
        bool is_hidden;
        da += my_depth;
        dc++;
        if (cur_depth == 0 || my_depth <= cur_depth) {
          // cur_depth = my_depth;
          // not hidden
          is_hidden = false;
        } else {
          is_hidden = true;
        }
        obj_vertex_hidden[idx] = is_hidden;
        //if (!is_hidden) {
        //  cout << idx << " Depths: " << cur_depth << " " << my_depth << "\n";
        //  cout << pointM64 << "\n";
        //  cout << rotptv << "\n";
        //}

        // deptht.at<float>(p[1], p[0]) = cur_depth;      
    }
    da = da / dc;
    //cout << "========================\n" << obj.xyzw << "\n";

    auto imt2 = imt.clone();
    auto imt3 = imt.clone();
    Mat birdMask(imt2.size(), CV_8UC1);
    birdMask.setTo(0);
    for (int i = 0; i < obj.triangles.size(); i++) {
      Point pts[1][3];
      int is_hidden = 0;
      for (int j = 0; j < 3; j++) {
        // If any of the vertices are hidden, skip this triangle
        if (obj_vertex_hidden[obj.triangles[i][j]]) {
          // Occluded
          is_hidden += 1;
          //continue;
        }
        Eigen::Vector2d pt;
        auto p3d = obj.vertices[obj.triangles[i][j]];
        c65.ProjectPoint(p3d, &pt);  
        pts[0][j] = Point(pt(0), pt(1));
      }

      const Point* ppt[1] = { pts[0] };
      int npt[] = { 3 };

      // Fill imt2 regardless
      fillPoly(imt2, ppt, npt, 1, Scalar(255, 0, 0));
      fillPoly(birdMask, ppt, npt, 1, 255);

      if (is_hidden) {
        //cout << "is_hidden: " << is_hidden << endl;
        continue;
      }
     
      fillPoly(imt, ppt, npt, 1, Scalar(255, 0, 0));
    }

    for(int y = 0; y < birdMask.rows; y++) {
      for(int x = 0; x < birdMask.cols; x++) {
        float cd = deptht.at<float>(y,x);
        int bm = birdMask.at<char>(y,x);
        if (bm == 0) {
          continue;
        }
        // Hack on the side of no occlusions
        if (cd == 0 || (da - 1.5) < cd) {
          //cout << cd << " " << da << endl;
          imt3.at<Vec3b>(y,x) = Vec3b(255,0,0);
        }
      }
    }
    //FileStorage fs("depth.yml", FileStorage::WRITE );
    //fs << "depth" << deptht;
    /*stringstream ss;
    ss << "depth/depth_" << frame << ".yml";
    string filename = ss.str();
    FileStorage fs(filename, FileStorage::WRITE );
    fs << "depth" << deptht;
    
    stringstream ss2;
    ss2 << "projuniform/projuniform_" << frame << ".jpg";
    filename = ss2.str();
    cv::imwrite(filename, imt3);

    stringstream ss3;
    ss3 << "projtriangle/projtriangle_" << frame << ".jpg";
    filename = ss3.str();
    cv::imwrite(filename, imt);

    
    stringstream ss4;
    ss4 << "depthi/depthi_" << frame << ".jpg";
    filename = ss4.str();
    Mat depth8;
    deptht.convertTo(depth8, CV_8U);
    cv::imwrite(filename, depth8);*/

    deptht = deptht / 250.0;
    cv::imshow("test", deptht);
    cv::imshow("orig", imt);
    cv::imshow("orig no occlusion", imt2);
    cv::imshow("samedepth", imt3);
    cv::imshow("bird", birdMask);
    waitKey();
  }

  
  /*
  // Let's display the point cloud
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  const double maxD = 5000;
  for (int i = 0; i < allPoints.size(); i++) {
    int idx = 0;
    const auto& xyzw = allPoints[i].xyzw;
    const auto& im1r = allPoints[i].im1r;
    for(int y = 0; y < im1r.rows; y++) {
        for(int x = 0; x < im1r.cols; x++) {
            const Vec3d& point = xyzw.at<Vec3d>(idx);
            Vec3b color = im1r.at<Vec3b>(y, x);
            PointXYZRGB pt(color[2], color[1], color[0]);
            pt.x = point[0];
            pt.y = point[1];
            pt.z = point[2];
            if (abs(pt.x) > maxD || abs(pt.y) > maxD || abs(pt.z) > maxD) {
              idx++;
              continue;
            }
            cloud->push_back(pt);
            idx++;
        }
    }
  }

  waitKey();
  pcl::visualization::CloudViewer viewer("Simple Cloud Viewer");
  viewer.showCloud(cloud);
  while (!viewer.wasStopped()) {
    sleep(10);
  }
  */
  

  reconstruction.release();
  return 0;
}
