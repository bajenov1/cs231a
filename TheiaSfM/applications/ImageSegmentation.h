#include <opencv2/core/core.hpp>

cv::Mat segment_image(cv::Mat const& img, int& num_segments);
cv::Mat smooth_depth_map_using_segmentation(cv::Mat imt, cv::Mat const& deptht, const cv::Mat& K);