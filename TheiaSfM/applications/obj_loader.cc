#include "obj_loader.h"

#include <sstream>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <iostream>

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;
  
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  
  return internal;
}

ObjLoader::ObjLoader(string filename, double scale) {
	const double tx = -10.0;
	const double ty = 0.0;
	const double tz = 10.0;
	ifstream f(filename);
	if (!f.is_open()) {
		throw runtime_error("File not found");
	}

	string line;
	while(getline(f, line)) {
		if (line.empty()) {
			continue;
		}

		//printf("%s\n", line.c_str());

		if (line[0] == 'v' && line[1] == ' ') {
			// Vertex
			auto tokens = split(line, ' ');
            double v1 = stod(tokens[1]);
            double v2 = -stod(tokens[2]);
            double v3 = stod(tokens[3]);
            v1 = scale * v1;
            v2 = scale * v2;
            v3 = scale * v3;
            v1 = v1 + tx;
            v2 = v2 + ty;
            v3 = v3 + tz;
            vertices.emplace_back(v1, v2, v3, 1);
		}

		if (line[0] == 'f' && line[1] == ' ') {
			// Triangle
			auto tokens = split(line, ' ');
			if (tokens.size() != 4) {
				throw runtime_error("Expected tokens to be 4");
			}
			
			vector<int> vertices;
			for (int i = 1; i < 4; i++) {
				auto n = split(tokens[i], '/');
				if (n.size() < 2) {
					throw runtime_error("Expected n to be >= 2");
				}
				
				vertices.emplace_back(stoi(n[0]));
			}
			triangles.emplace_back(vertices[0]-1, vertices[1]-1, vertices[2]-1);
		}
	}

	f.close();

	setXYZW();
}

void ObjLoader::setXYZW() {
	xyzw = Mat(1, vertices.size(), CV_64FC3);
	for (int i = 0; i < vertices.size(); i++) {
		double p1 = vertices[i][0];
		double p2 = vertices[i][1];
		double p3 = vertices[i][2];
		double p4 = vertices[i][3];
		Vec3d pt(p1/p4, p2/p4, p3/p4);
		xyzw.at<Vec3d>(i) = pt;
	}	
}

void ObjLoader::makeSmallerTriangles() {
	// Take the centroid of each triangle and make 3 triangles

	// Each triangle, we add a new vertex, its centroid
	vertices.reserve(vertices.size() + triangles.size());

	// We have 3 times as many triangles than before
	decltype(triangles) smaller_triangles;
	smaller_triangles.reserve(3 * triangles.size());
	
	for (int i = 0; i < triangles.size(); i++) {
		int v1idx = triangles[i][0];
		int v2idx = triangles[i][1];
		int v3idx = triangles[i][2];
		auto v1 = vertices[v1idx];
		auto v2 = vertices[v2idx];
		auto v3 = vertices[v3idx];
		auto centroid = (v1 + v2 + v3) / 3.0;
		int cidx = vertices.size();
		vertices.emplace_back(centroid);
		smaller_triangles.emplace_back(v1idx, v2idx, cidx);
		smaller_triangles.emplace_back(v1idx, v3idx, cidx);
		smaller_triangles.emplace_back(v2idx, v3idx, cidx);
	}
	
	// Set the class member variable
	triangles = smaller_triangles;

	// Make sure to set the xyzw matrix correctly
	setXYZW();
}