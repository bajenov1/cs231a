// Copyright (C) 2014 The Regents of the University of California (Regents).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of The Regents or University of California nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// Author: Chris Sweeney (cmsweeney@cs.ucsb.edu)

#include <Eigen/Core>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <theia/theia.h>
#include <string>
#include <vector>

DEFINE_string(reconstruction, "", "Reconstruction file to be viewed.");

#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include "theia/sfm/types.h"


#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#include "libelas/src/elas.h"
#include "libelas/src/image.h"

#include "opencv2/viz/types.hpp"

#include "obj_loader.h"

using namespace std;
using namespace cv;
using namespace cv::sfm;
using namespace theia;
using namespace pcl;
// Containers for the data.
std::vector<theia::Camera> cameras;
std::vector<Eigen::Vector3d> world_points;
std::vector<Eigen::Vector3f> point_colors;
std::vector<int> num_views_for_track;

//DEFINE_double(obj_scale, 2, "Multiplier for model");

void saveXYZ(const char* filename, const Mat& mat)
{
    const double max_z = 1.0e4;
    FILE* fp = fopen(filename, "wt");
    for(int y = 0; y < mat.rows; y++)
    {
        for(int x = 0; x < mat.cols; x++)
        {
            Vec3f point = mat.at<Vec3f>(y, x);
            if(fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z) continue;
            fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
        }
    }
    fclose(fp);
}

int main(int argc, char* argv[]) {
  THEIA_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  //ObjLoader obj("../../models/m2/Soccer Ball/Soccer Ball.obj", 0.05);
  ObjLoader obj("../../models/m1/144.obj", 1);

  // Output as a binary file.
  std::unique_ptr<theia::Reconstruction> reconstruction(
      new theia::Reconstruction());
  CHECK(ReadReconstruction(FLAGS_reconstruction, reconstruction.get()))
      << "Could not read reconstruction file.";

  // Centers the reconstruction based on the absolute deviation of 3D points.
  reconstruction->Normalize();

  map<string, Mat> images;
  map<theia::ViewId, Mat> images_in_order;
  map<string, Eigen::Matrix3d> Rs;
  map<string, Eigen::Vector3d> Ts;
  map<string, Eigen::Matrix3d> Ks;
  map<string, Matrix3x4d> Ms;
  double rd1 = 0;
  double rd2 = 0;
  // Set up camera drawing.
  cameras.reserve(reconstruction->NumViews());
  for (const theia::ViewId view_id : reconstruction->ViewIds()) {
    const auto* view = reconstruction->View(view_id);
    if (view == nullptr || !view->IsEstimated()) {
      continue;
    }
    Mat im = imread(string("/home/andreib/cs231/cs231a/media/sequence2/") + view->Name());
    theia::Matrix3x4d proj;
    view->Camera().GetProjectionMatrix(&proj);
    cameras.emplace_back(view->Camera());
    images[view->Name()] = im;

    for (int i = 0; i < obj.triangles.size(); i++) {
      Point pts[1][3];
      for (int j = 0; j < 3; j++) {
        Eigen::Vector2d pt;
        auto p3d = obj.vertices[obj.triangles[i][j]];
        view->Camera().ProjectPoint(p3d, &pt);  
        pts[0][j] = Point(pt(0), pt(1));
      }
      const Point* ppt[1] = { pts[0] };
      int npt[] = { 3 };
      
      fillPoly(im, ppt, npt, 1, Scalar(255, 0, 0)); 
      //circle(im, Point(pt(0), pt(1)), 1, Scalar( 255, 0, 0), 1);
    }
    
    images_in_order[view_id] = im;


    //////
    const auto& camera = view->Camera();
    Eigen::Matrix3d R = camera.GetOrientationAsRotationMatrix();
    Eigen::Vector3d T = camera.GetPosition();
    Eigen::Matrix3d K;
    camera.GetCalibrationMatrix(&K);
    Matrix3x4d M;
    camera.GetProjectionMatrix(&M);
    Rs[view->Name()] = R;
    Ts[view->Name()] = T;
    Ks[view->Name()] = K;
    Ms[view->Name()] = M;
    rd1 = camera.RadialDistortion1();
    rd2 = camera.RadialDistortion2();
    /////
  }

  int frame = 350;
  string namet = string("seq") + to_string(frame) + ".jpg";
  {
    auto im = images[namet];
    imshow("specific frame", im);
    waitKey();
  }

  for (auto const& kv: images_in_order) {
    auto im = kv.second;
    imshow("projected", im);
    waitKey();
  }
  

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  for (int i = 0, j = i + 30; j < reconstruction->NumViews(); i++, j++) {
    cout << "Frame " << i << " " << j << endl;
    string name1 = string("seq") + to_string(j) + ".jpg";
    string name2 = string("seq") + to_string(i) + ".jpg";
    auto eK1 = Ks[name1];
    Mat K1;
    eigen2cv(eK1, K1);
    auto eR1 = Rs[name1];
    Mat R1;
    eigen2cv(eR1, R1);
    Mat R1temp;
    transpose(R1, R1temp);

    auto eT1 = Ts[name1];
    Mat T1temp;
    eigen2cv(eT1, T1temp);
    Mat T1 = - R1 * T1temp;

    auto eM1 = Ms[name1];
    Mat M1;
    eigen2cv(eM1, M1);

    auto eK2 = Ks[name2];
    Mat K2;
    eigen2cv(eK2, K2);
    auto eR2 = Rs[name2];
    Mat R2;
    eigen2cv(eR2, R2);
    Mat R2temp;
    transpose(R2, R2temp);


    auto eT2 = Ts[name2];
    Mat T2temp;
    eigen2cv(eT2, T2temp);
    Mat T2 = - R2 * T2temp;

    auto eM2 = Ms[name2];
    Mat M2;
    eigen2cv(eM2, M2);

    /*
    cout << "stuff" << endl;
    cout << R1 << endl;
    cout << T1 << endl;
    cout << "mult" << endl;
    cout << (K1 * R1) << endl;
    cout << (K1 * T1) << endl;
    cout << "M" << endl;
    cout << M1 << endl;
    fflush(stdout);
    waitKey();
    */

    Mat Ra1;
    Mat Ra2;
    Mat P1;
    Mat P2;
    Mat Q;
    Mat d1 = Mat::zeros(4,1, CV_64F);
    d1.at<double>(0,0) = rd1;
    d1.at<double>(1,0) = rd2;
    Mat d2 = d1;
    Mat R1t;
    transpose(R1, R1t);
    Mat R = R2 * R1t;
    Mat T = T2 - R2 * R1t * T1;
    Mat im1 = images[name1];
    Mat im2 = images[name2];
    /*
    cout << R1 << endl;
    cout << R2 << endl;
    cout << T1 << endl;
    cout << T2 << endl;
    cout << K1 << endl;
    cout << K2 << endl;
    cout << R << endl;
    cout << T << endl;
    */

    Rect roi1, roi2;
    stereoRectify(K1, d1, K2, d2, im1.size(), R, T, Ra1, Ra2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, im1.size(), &roi1, &roi2);

    Mat map11, map12, map21, map22;
    initUndistortRectifyMap(K1, d1, Ra1, P1, im1.size(), CV_16SC2, map11, map12);
    initUndistortRectifyMap(K2, d2, Ra2, P2, im1.size(), CV_16SC2, map21, map22);

    Mat img1r, img2r;
    remap(im1, img1r, map11, map12, INTER_LINEAR);
    remap(im2, img2r, map21, map22, INTER_LINEAR);

    im1 = img1r;
    im2 = img2r;

    Mat eim1;
    Mat eim2;
    cv::cvtColor(im1, eim1, CV_BGR2GRAY);
    cv::cvtColor(im2, eim2, CV_BGR2GRAY);

  cv::imshow("1", eim1);
  cv::imshow("2", eim2);

  const int32_t dims[3] = {im1.cols,im1.rows,im1.cols};
  Mat dim1(im1.rows, im1.cols, CV_32FC1);
  Mat dim2(im2.rows, im2.cols, CV_32FC1);

  Elas::parameters param;
  param.postprocess_only_left = false;
  Elas elas(param);
  elas.process(eim1.data, eim2.data, (float *) dim1.data, (float *) dim2.data, dims);
  //normalize(eim1, eim1, 0.0, 1.0, CV_MINMAX);
  //normalize(eim2, eim2, 0.0, 1.0, CV_MINMAX);

double min, max;
cv::minMaxLoc(dim1, &min, &max);
dim1 = dim1 / max;
cv::minMaxLoc(dim2, &min, &max);
dim2 = dim2 / max;

  cv::imshow("dim1", dim1);
  cv::imshow("dim2", dim2);

    enum { STEREO_BM=0, STEREO_SGBM=1, STEREO_HH=2, STEREO_VAR=3, STEREO_3WAY=4 };
    int alg = STEREO_SGBM;

    //Ptr<StereoBM> bm = StereoBM::create(16,9);
    Ptr<StereoSGBM> sgbm = StereoSGBM::create(0,16,3);

    int numberOfDisparities = ((im1.size().width/8) + 15) & -16;

    /*
    bm->setROI1(roi1);
    bm->setROI2(roi2);
    bm->setPreFilterCap(31);
    bm->setBlockSize(9);
    bm->setMinDisparity(0);
    bm->setNumDisparities(numberOfDisparities);
    bm->setTextureThreshold(10);
    bm->setUniquenessRatio(15);
    bm->setSpeckleWindowSize(100);
    bm->setSpeckleRange(32);
    bm->setDisp12MaxDiff(1);
    */

    sgbm->setPreFilterCap(63);
    int sgbmWinSize = 3;
    sgbm->setBlockSize(sgbmWinSize);

    int cn = im1.channels();

    sgbm->setP1(8*cn*sgbmWinSize*sgbmWinSize);
    sgbm->setP2(32*cn*sgbmWinSize*sgbmWinSize);
    sgbm->setMinDisparity(0);
    sgbm->setNumDisparities(numberOfDisparities);
    sgbm->setUniquenessRatio(10);
    sgbm->setSpeckleWindowSize(100);
    sgbm->setSpeckleRange(32);
    sgbm->setDisp12MaxDiff(1);
    if(alg==STEREO_HH)
        sgbm->setMode(StereoSGBM::MODE_HH);
    else if(alg==STEREO_SGBM)
        sgbm->setMode(StereoSGBM::MODE_SGBM);
    else if(alg==STEREO_3WAY)
        sgbm->setMode(StereoSGBM::MODE_SGBM_3WAY);

    Mat disp, disp8;
    //Mat img1p, img2p, dispp;
    //copyMakeBorder(im1, img1p, 0, 0, numberOfDisparities, 0, IPL_BORDER_REPLICATE);
    //copyMakeBorder(im2, img2p, 0, 0, numberOfDisparities, 0, IPL_BORDER_REPLICATE);

    int64 t = getTickCount();
    if( alg == STEREO_BM ) {
      //bm->compute(im1, im2, disp);
      throw std::runtime_error("Not supported");
    } else if( alg == STEREO_SGBM || alg == STEREO_HH || alg == STEREO_3WAY ) {
      sgbm->compute(im1, im2, disp);
    }

    t = getTickCount() - t;
    printf("Time elapsed: %fms\n", t*1000/getTickFrequency());
    //cout << numberOfDisparities << " " << img1p.cols << endl;
    //disp = dispp.colRange(numberOfDisparities, img1p.cols);
    if( alg != STEREO_VAR ) {
      disp.convertTo(disp8, CV_8U, 255/(numberOfDisparities*16.));
    } else {
      disp.convertTo(disp8, CV_8U);
    }

    
    namedWindow("left", 1);
    cv::imshow("left", im1);
    namedWindow("right", 1);
    cv::imshow("right", im2);
    namedWindow("disparity", 0);
    cv::imshow("disparity", disp8);
    printf("press any key to continue...");
    fflush(stdout);
    waitKey();
    printf("\n");
    

    //printf("storing the point cloud...");
    //fflush(stdout);
    Mat xyz;
    reprojectImageTo3D(disp, xyz, Q, true);
    //saveXYZ(point_cloud_filename.c_str(), xyz);
    //printf("\n");

    const double max_z = 1.0e4;
    //FILE* fp = fopen(filename, "wt");
    for(int y = 0; y < xyz.rows; y++)
    {
        for(int x = 0; x < xyz.cols; x++)
        {
            Vec3f point = xyz.at<Vec3f>(y, x);
            if(fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z) continue;
            //fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
            cloud->push_back(PointXYZ(point[0], point[1], point[2]));
        }
    }
    //fclose(fp);

    //... populate cloud
    /*
    ifstream points_file(point_cloud_filename);
    float x, y, z;
    while (points_file >> x >> y >> z) {
        cloud->push_back(PointXYZ(x, y, z));
    }
    */

    //cv::imshow("a1", img1r);
    //cv::imshow("a2", img2r);
    //waitKey();

    //cout << Q << endl;
    /*
    for (int i = 0; i < images.size(); i++) {
      string name = string("seq") + to_string(i) + ".jpg";
      Mat im = images[name];
      cv::imshow("seq", im);
      waitKey();
    }
    */
  }

  pcl::visualization::CloudViewer viewer("Simple Cloud Viewer");
  viewer.showCloud(cloud);
  while (!viewer.wasStopped()) {
    sleep(10);
  }

  /*
  // Set up world points and colors.
  world_points.reserve(reconstruction->NumTracks());
  point_colors.reserve(reconstruction->NumTracks());
  for (const theia::TrackId track_id : reconstruction->TrackIds()) {
    const auto* track = reconstruction->Track(track_id);
    if (track == nullptr || !track->IsEstimated()) {
      continue;
    }
    world_points.emplace_back(track->Point().hnormalized());
    point_colors.emplace_back(track->Color().cast<float>());
    num_views_for_track.emplace_back(track->NumViews());
  }
  */

  reconstruction.release();
  return 0;
}
