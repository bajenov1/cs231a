// Copyright (C) 2014 The Regents of the University of California (Regents).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of The Regents or University of California nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// Author: Chris Sweeney (cmsweeney@cs.ucsb.edu)

#include <Eigen/Core>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <theia/theia.h>
#include <string>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#ifdef FREEGLUT
#include <GL/freeglut.h>
#else  // FREEGLUT
#include <GLUT/glut.h>
#endif  // FREEGLUT
#else  // __APPLE__
#ifdef _WIN32
#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#else  // _WIN32
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif  // _WIN32
#endif  // __APPLE__

DEFINE_string(reconstruction, "", "Reconstruction file to be viewed.");

#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include "theia/sfm/types.h"


#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#include "libelas/src/elas.h"
#include "libelas/src/image.h"

#include "opencv2/viz/types.hpp"

#include "opencv2/ximgproc/disparity_filter.hpp"

using namespace std;
using namespace cv;
using namespace cv::sfm;
using namespace cv::viz;
using namespace theia;
using namespace pcl;
// Containers for the data.
std::vector<theia::Camera> cameras;
std::vector<Eigen::Vector3d> world_points;
std::vector<Eigen::Vector3f> point_colors;
std::vector<int> num_views_for_track;

int main(int argc, char* argv[]) {
  THEIA_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  // Output as a binary file.
  std::unique_ptr<theia::Reconstruction> reconstruction(
      new theia::Reconstruction());
  CHECK(ReadReconstruction(FLAGS_reconstruction, reconstruction.get()))
      << "Could not read reconstruction file.";

  // Centers the reconstruction based on the absolute deviation of 3D points.
  reconstruction->Normalize();

  map<string, Mat> images;
  map<string, View> Vs;
  map<string, theia::Camera> viewToCamera;
  // Set up camera drawing.
  cameras.reserve(reconstruction->NumViews());
  for (const theia::ViewId view_id : reconstruction->ViewIds()) {
    const auto* view = reconstruction->View(view_id);
    if (view == nullptr || !view->IsEstimated()) {
      continue;
    }
    Mat im = imread(string("/home/andreib/cs231/cs231a/media/sequence2/") + view->Name());
    theia::Matrix3x4d proj;
    view->Camera().GetProjectionMatrix(&proj);
    cameras.emplace_back(view->Camera());

    Eigen::Vector2d pt;
    view->Camera().ProjectPoint(Eigen::Vector4d(0,0,0,1), &pt);
    // circle(im, Point(pt(0), pt(1)), 1, Scalar( 255, 0, 0), 50);
    images[view->Name()] = im;
    Vs[view->Name()] = *view;
    viewToCamera[view->Name()] = view->Camera();
    /////
  }
  
  VideoCapture cap("../../media/IMG_5110.m4v");
  if (!cap.isOpened()) {
      return -1;
  }

  // Setup output video
  cv::VideoWriter output_cap("outvideo.m4v", 
                 cap.get(CV_CAP_PROP_FOURCC),
                 cap.get(CV_CAP_PROP_FPS),
                 cv::Size(cap.get(CV_CAP_PROP_FRAME_WIDTH),
                 cap.get(CV_CAP_PROP_FRAME_HEIGHT)));

  if (!output_cap.isOpened())
  {
          std::cout << "!!! Output video could not be opened" << std::endl;
          return 0;
  }


  for (int frame = 30; frame < 421; frame += 1) {
    cout << "Frame: " << frame << endl;
    string namet = string("seq") + to_string(frame) + ".jpg";
    Mat im_orig = images[namet];
    //const View& viewt = Vs[namet];
    theia::Camera c65 = viewToCamera[namet];

    Mat deptht, im_uniform, im_triangle, depth8;
    string filename;

    stringstream ss2;
    ss2 << "projuniform/projuniform_" << frame << ".jpg";
    filename = ss2.str();
    im_uniform = cv::imread(filename);

    /*
    stringstream ss;
    ss << "../../../results/depth/depth_" << frame << ".yml";
    filename = ss.str();
    FileStorage fs(filename, FileStorage::READ );
    fs["depth"] >> deptht;
    
    stringstream ss3;
    ss3 << "../../../results/projtriangle/projtriangle_" << frame << ".jpg";
    filename = ss3.str();
    im_triangle = cv::imread(filename);
    
    stringstream ss4;
    ss4 << "../../../results/depthi/depthi_" << frame << ".jpg";
    filename = ss4.str();
    depth8 = cv::imread(filename);
    */

    output_cap.write(im_uniform);
    //cv::imshow("Reprojected", im_uniform);
    //waitKey(50);
  }

  reconstruction.release();
  return 0;
}
