#include "ImageSegmentation.h"

#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>
#include <unordered_map>
#include <iostream>

using namespace cv;
using namespace std;

// Given a set of points, find the plane they all lie on
// points: N x 3 matrix of points
Vec4f computePlane(Mat points, double& norm1) {
  int N = points.rows;
  /*Mat centroid;

  // Compute the centroid
  reduce(points, centroid, 0, CV_REDUCE_SUM);
  centroid = centroid / N;

  // Repeat the centroid
  Mat centroid_mat;
  repeat(centroid, 0, N, centroid_mat);

  // Subtract the centroid from the points
  Mat normalized_points = points - centroid_mat;
  */
  // ax + by + cz + 1 = 0
  Mat bt = -1 * Mat::ones(N, 1, 1);
  Mat b;
  bt.convertTo(b, CV_32F);
  Mat A = points;

  // Solve using svd
  Mat solution; // 3 x 1
  bool success = solve(A, b, solution, DECOMP_SVD);
  if (!success) {
    throw runtime_error("Could not solve Ax=b in computePlane()");
  }

  Mat diff = A * solution - b;
  norm1 = norm(diff, NORM_L2);

  Vec4f plane;
  plane[0] = solution.at<float>(0, 0);
  plane[1] = solution.at<float>(1, 0);
  plane[2] = solution.at<float>(2, 0);
  plane[3] = 1;
  return plane;
}

void showImage(string const& name, Mat img) {
  namedWindow(name, WINDOW_NORMAL);
  //Mat small_img;
  //resize(img, small_img, Size(), 0.25, 0.25);
  imshow(name, img);
}

Mat createSegmentationDisplay(Mat& segments, int numOfSegments, Mat const& image)
{
  // Create a new image
  Mat wshed(segments.size(), CV_8UC3);
  
  //Create color tab for coloring the segments
  vector<Vec3b> colorTab;
  for(int i = 0; i < numOfSegments; i++) {
    int b = theRNG().uniform(0, 255);
    int g = theRNG().uniform(0, 255);
    int r = theRNG().uniform(0, 255);
    colorTab.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
  }
  
  // Assign different color to different segments
  for(int i = 0; i < segments.rows; i++) {
    for(int j = 0; j < segments.cols; j++) {
      int index = segments.at<int>(i,j);
      if(index == -1) {
        wshed.at<Vec3b>(i,j) = Vec3b(255,255,255);
      }
      else if(index <= 0 || index > numOfSegments) {
        wshed.at<Vec3b>(i,j) = Vec3b(0,0,0);
      }
      else {
        wshed.at<Vec3b>(i,j) = colorTab[index - 1]; 
      }
    }
  }

  //If the original image available then merge with the colors of segments
  if(image.dims > 0) {
    //wshed = (wshed * 0.5) + (image * 0.5);
  }

  return wshed;
}

Mat segment_image(Mat const& image, int& num_segments) {
  Mat gray, thresholded;

  // Convert the image to grayscale
  cvtColor(image, gray, CV_BGR2GRAY);
  //showImage("Gray Image", gray);
  
  // Theshold the image

  // OTSU thresholding
  // threshold(gray, thresholded, 0, 255, CV_THRESH_BINARY_INV + CV_THRESH_OTSU);
  // showImage("Image after OTSU Thresholding", thresholded);
  
  // Adaptive thresholding
  //adaptiveThreshold(gray, thresholded, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 101, 0);
  adaptiveThreshold(gray, thresholded, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 31, 0);
  //showImage("Image after adaptive thresholding", thresholded);

  // Execute morphological-open
  morphologyEx(thresholded, thresholded, MORPH_OPEN, Mat::ones(5, 5, CV_8SC1), Point(2, 2), 2);
  //showImage("Thresholded Image after Morphological open", thresholded);
  
  // Get the distance transformation 
  Mat distTransformed(thresholded.rows, thresholded.cols, CV_32FC1);
  distanceTransform(thresholded, distTransformed, CV_DIST_L2, 3);
  
  // Normalize the transformed image
  normalize(distTransformed, distTransformed, 0.0, 1, NORM_MINMAX);
  //showImage("Distance Transformation", distTransformed);
  
  // Threshold the transformed image to obtain markers for watershed
  threshold(distTransformed, distTransformed, 0.2, 1, CV_THRESH_BINARY);
  // Renormalize to 0-255 to further calculations
  normalize(distTransformed, distTransformed, 0.0, 255.0, NORM_MINMAX);
  
  distTransformed.convertTo(distTransformed, CV_8UC1);
  //showImage("Thresholded Distance Transformation", distTransformed);

  /*
  Mat dst, detected_edges;

  //int edgeThresh = 1;
  int lowThreshold = 10;
  //int const max_lowThreshold = 100;
  int ratio = 3;
  int kernel_size = 3;

  /// Create a matrix of the same type and size as src (for dst)
  dst.create(image.size(), image.type());

  /// Reduce noise with a kernel 3x3
  blur(gray, detected_edges, Size(3,3));

  /// Canny detector
  Canny(detected_edges, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size);

  /// Using Canny's output as a mask, we display our result
  dst = Scalar::all(0);

  image.copyTo(dst, detected_edges);
  //cv::imshow("Edge Map", detected_edges);
  
  Mat edges;
  detected_edges.convertTo(edges, CV_8UC1);

  cv::imshow("Edge Map 2", edges);

  Mat combined, combined2;
  edges.convertTo(combined, CV_32FC1);
  distTransformed.convertTo(combined2, CV_32FC1);

  combined2 = combined2 + combined;
  threshold(combined2, combined2, 255.0, 255.0, CV_THRESH_BINARY);
  normalize(combined2, combined2, 0.0, 255.0, NORM_MINMAX);
  combined2.convertTo(distTransformed, CV_8UC1);
  cv::imshow("Distance tranform + edges", distTransformed);
  */

  // Calculate the contours of markers
  int compCount = 0;
  vector<vector<Point>> contours;
  vector<Vec4i> hierarchy;
  findContours(distTransformed, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

  if(contours.empty()) {
    return Mat();
  }
    
  Mat markers(distTransformed.size(), CV_32S);
  markers = Scalar::all(0);

  // Draw contours 
  for(int idx = 0; idx >= 0; idx = hierarchy[idx][0], compCount++) {
    drawContours(markers, contours, idx, Scalar::all(compCount + 1), -1, 8, hierarchy, INT_MAX);
  }
  
  if(compCount == 0) {
      return Mat();
  }

  // Apply watershed with the markers as seeds
  watershed(image, markers);

  // Create displayable image of segments
  Mat wshed = createSegmentationDisplay(markers, compCount, image);

  showImage("watershed transform", wshed);
  num_segments = compCount;

  //returns the segments
  return markers;
}

cv::Mat smooth_depth_map_using_segmentation(cv::Mat im, cv::Mat const& depth, const cv::Mat& K) {
  int num_segments;
  auto markers = segment_image(im, num_segments);
  // For each segment, take the median of all the pixels
  // Marker id -> list of depths
  unordered_map<int, vector<double>> segment_to_depths;
  unordered_map<int, vector<Point>> segment_to_points;

  for (int y = 0; y < markers.rows; y++) {
    for (int x = 0; x < markers.cols; x++) {
      auto m = markers.at<int>(y, x);
      auto d = depth.at<float>(y, x);
      if (d == 0) {
        //continue;
      }
      if (m == -1 && x > 0) {
        m = markers.at<int>(y, x-1);
        markers.at<int>(y, x) = m;
      }
      if (m == -1) {
        continue;
      }
      segment_to_depths[m].push_back(d);
      Point p;
      p.x = x;
      p.y = y;
      segment_to_points[m].push_back(p);
    }
  }

  auto smooth_depths = depth.clone();
  //Mat smooth_depths(depth.size(), depth.type());
  //smooth_depths.setTo(0);
  for (auto& kv: segment_to_points) {
    auto& seg = kv.first;
    auto& pts = kv.second;
    auto& depths = segment_to_depths[seg];
    int nonz = 0;
    for (int i = 0; i < depths.size(); i++) {
      if (depths[i] != 0) {
        nonz++;
      }
    }
    if (nonz < 3) {
      continue;
    }
    Mat points(nonz, 3, CV_32FC1);
    int idx = 0;
    for (int i = 0; i < pts.size(); i++) {
      const Point& pt = pts[i];
      float fx = K.at<double>(0,0);
      float fy = K.at<double>(1,1);
      float cx = K.at<double>(0,2);
      float cy = K.at<double>(1,2);
      float px = pt.x;
      float py = pt.y;
      float z = segment_to_depths[seg][i];
      if (z == 0) {
        continue;
      }
      points.at<Vec3f>(idx) = Vec3f(z*(py - cy) / fy, z*(px - cx) / fx, z);
      idx++;
    }
    Vec4f plane;
    double norm;
    try {
      plane = computePlane(points, norm);
      // Really bad fit, don't proceed, something is wrong.
      if (norm > 7.0) {
        continue;
      }
    } catch(...) {
      continue;
    }

    for (int i = 0; i < pts.size(); i++) {
      const Point& pt = pts[i];
      float fx = K.at<double>(0,0);
      float fy = K.at<double>(1,1);
      float cx = K.at<double>(0,2);
      float cy = K.at<double>(1,2);
      float px = pt.x;
      float py = pt.y;
      Vec3f v((py - cy) / fy, (px - cx) / fx, 1.0);
      float t = - plane[3] / (plane[0] * v[0] + plane[1] * v[1] + plane[2] * v[2]);
      float newz = t;
      smooth_depths.at<float>(py,px) = newz;
    }
  }

  return smooth_depths;
}