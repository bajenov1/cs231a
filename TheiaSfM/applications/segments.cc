// Copyright (C) 2014 The Regents of the University of California (Regents).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
//     * Neither the name of The Regents or University of California nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Please contact the author of this library if you have any questions.
// Author: Chris Sweeney (cmsweeney@cs.ucsb.edu)

#include <Eigen/Core>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <theia/theia.h>
#include <string>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#ifdef FREEGLUT
#include <GL/freeglut.h>
#else  // FREEGLUT
#include <GLUT/glut.h>
#endif  // FREEGLUT
#else  // __APPLE__
#ifdef _WIN32
#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#else  // _WIN32
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif  // _WIN32
#endif  // __APPLE__

DEFINE_string(reconstruction, "", "Reconstruction file to be viewed.");

#include <opencv2/sfm.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include <opencv2/imgproc/imgproc.hpp>

#include "theia/sfm/types.h"


#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"

using namespace std;
using namespace cv;
using namespace cv::sfm;
using namespace theia;

Mat createSegmentationDisplay(Mat& segments, int numOfSegments, Mat& image)
{
  // Create a new image
  Mat wshed(segments.size(), CV_8UC3);
  
  //Create color tab for coloring the segments
  vector<Vec3b> colorTab;
  for(int i = 0; i < numOfSegments; i++) {
    int b = theRNG().uniform(0, 255);
    int g = theRNG().uniform(0, 255);
    int r = theRNG().uniform(0, 255);
    colorTab.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
  }
  
  // Assign different color to different segments
  for(int i = 0; i < segments.rows; i++) {
    for(int j = 0; j < segments.cols; j++) {
      int index = segments.at<int>(i,j);
      if(index == -1) {
        wshed.at<Vec3b>(i,j) = Vec3b(255,255,255);
      }
      else if(index <= 0 || index > numOfSegments) {
        wshed.at<Vec3b>(i,j) = Vec3b(0,0,0);
      }
      else {
        wshed.at<Vec3b>(i,j) = colorTab[index - 1]; 
      }
    }
  }

  //If the original image available then merge with the colors of segments
  if(image.dims > 0) {
    wshed = (wshed * 0.5) + (image * 0.5);
  }

  return wshed;
}

Mat segment_image(Mat& image, int& num_segments) {
  Mat gray, thresholded;

  // Convert the image to grayscale
  cvtColor(image, gray, CV_BGR2GRAY);
  //showImage("Gray Image", gray);
  
  // Theshold the image

  // OTSU thresholding
  // threshold(gray, thresholded, 0, 255, CV_THRESH_BINARY_INV + CV_THRESH_OTSU);
  // showImage("Image after OTSU Thresholding", thresholded);
  
  // Adaptive thresholding
  //adaptiveThreshold(gray, thresholded, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 101, 0);
  adaptiveThreshold(gray, thresholded, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 201, 0);
  //showImage("Image after adaptive thresholding", thresholded);

  // Execute morphological-open
  morphologyEx(thresholded, thresholded, MORPH_OPEN, Mat::ones(9, 9, CV_8SC1), Point(4,4), 2);
  //showImage("Thresholded Image after Morphological open", thresholded);
  
  // Get the distance transformation 
  Mat distTransformed(thresholded.rows, thresholded.cols, CV_32FC1);
  distanceTransform(thresholded, distTransformed, CV_DIST_L2, 3);
  
  // Normalize the transformed image
  normalize(distTransformed, distTransformed, 0.0, 1, NORM_MINMAX);
  //showImage("Distance Transformation", distTransformed);
  
  // Threshold the transformed image to obtain markers for watershed
  threshold(distTransformed, distTransformed, 0.1, 1, CV_THRESH_BINARY);
  // Renormalize to 0-255 to further calculations
  normalize(distTransformed, distTransformed, 0.0, 255.0, NORM_MINMAX);
  
  distTransformed.convertTo(distTransformed, CV_8UC1);
  //showImage("Thresholded Distance Transformation", distTransformed);
    
  // Calculate the contours of markers
  int compCount = 0;
  vector<vector<Point>> contours;
  vector<Vec4i> hierarchy;
  findContours(distTransformed, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

  if(contours.empty()) {
    return Mat();
  }
    
  Mat markers(distTransformed.size(), CV_32S);
  markers = Scalar::all(0);

  // Draw contours 
  for(int idx = 0; idx >= 0; idx = hierarchy[idx][0], compCount++) {
    drawContours(markers, contours, idx, Scalar::all(compCount + 1), -1, 8, hierarchy, INT_MAX);
  }
  
  if(compCount == 0) {
      return Mat();
  }

  // Apply watershed with the markers as seeds
  watershed(image, markers);

  // Create displayable image of segments
  Mat wshed = createSegmentationDisplay(markers, compCount, image);

  //showImage("watershed transform", wshed);
  num_segments = compCount;

  //returns the segments
  return markers;
}

// Containers for the data.
std::vector<theia::Camera> cameras;
std::vector<Eigen::Vector3d> world_points;
std::vector<Eigen::Vector3f> point_colors;
std::vector<int> num_views_for_track;


int main(int argc, char* argv[]) {
  THEIA_GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);

  // Output as a binary file.
  std::unique_ptr<theia::Reconstruction> reconstruction(
      new theia::Reconstruction());
  CHECK(ReadReconstruction(FLAGS_reconstruction, reconstruction.get()))
      << "Could not read reconstruction file.";

  // Centers the reconstruction based on the absolute deviation of 3D points.
  reconstruction->Normalize();

  map<string, View> views;

  // Set up camera drawing.
  cameras.reserve(reconstruction->NumViews());
  for (const theia::ViewId view_id : reconstruction->ViewIds()) {
    const auto* view = reconstruction->View(view_id);
    if (view == nullptr || !view->IsEstimated()) {
      continue;
    }
    views[view->Name()] = *view;
  }

  for (int i = 0; i < views.size(); i++) {
    string name = string("seq") + to_string(i) + ".jpg";
    Mat im = imread(string("/home/andreib/cs231/cs231a/media/sequence2/") + name);
    ////
    const auto& view = views[name];
    const auto& tracks = view.TrackIds();
    int numOfSegments = 0;
    Mat segments = segment_image(im, numOfSegments);
    Mat wshed = createSegmentationDisplay(segments, numOfSegments, im);
    im = wshed;
    for (const auto& id : tracks) {
      const Feature* f = view.GetFeature(id);
      Mat mp;
      eigen2cv(*f, mp);
      Point p;
      p.x = mp.at<double>(0);
      p.y = mp.at<double>(1);
      drawMarker(im, p, Scalar( 255, 0, 0));
    }
    ////
    imshow("seq", im);
    waitKey();
  }

  // Set up world points and colors.
  world_points.reserve(reconstruction->NumTracks());
  point_colors.reserve(reconstruction->NumTracks());
  for (const theia::TrackId track_id : reconstruction->TrackIds()) {
    const auto* track = reconstruction->Track(track_id);
    if (track == nullptr || !track->IsEstimated()) {
      continue;
    }
    world_points.emplace_back(track->Point().hnormalized());
    point_colors.emplace_back(track->Color().cast<float>());
    num_views_for_track.emplace_back(track->NumViews());
  }

  reconstruction.release();
  return 0;
}
