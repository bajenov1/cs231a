#pragma once

#include <opencv2/core/core.hpp>
using namespace cv;

namespace corner {
/** @function cornerHarris_demo */
void cornerHarris_detect(Mat src, Mat src_gray, Mat& corners, int thresh = 40);


void shiTomasiDetector(Mat& src, Mat& copy, int maxCorners=200);

bool detectHarrisKeyPoints(Mat& dst_norm, Mat& dst_norm_scaled, int thresh = 50, float scale_factor = 1.1);
}
