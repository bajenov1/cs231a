// Source: http://www.codeproject.com/Articles/751744/Image-Segmentation-using-Unsupervised-Watershed-Al

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <vector>

using namespace cv;
using namespace std;

Mat watershedSegment(Mat & image, int & noOfSegments);
Mat createSegmentationDisplay(Mat & segments, int numOfSegments, Mat & image);

void showImage(string const& name, Mat img) {
  namedWindow(name, WINDOW_NORMAL);
  Mat small_img;
  resize(img, small_img, Size(), 0.25, 0.25);
  imshow(name, small_img);
}

int main(int argc, char* argv[])
{
  string filename = "../media/room.jpg";
  //Read the file
  Mat image = imread(filename, CV_LOAD_IMAGE_COLOR);  
  //show original image
  //showImage("Original Image",image);
  
  // To store the number of segments
  int numOfSegments = 0; 
  // Apply watershed
  Mat segments = watershedSegment(image, numOfSegments);

  // To display the merged segments
  Mat wshed = createSegmentationDisplay(segments, numOfSegments, image);
  //Display the merged segments
  showImage("Merged segments", wshed);

  waitKey(0);

  return 0;
}

Mat watershedSegment(Mat & image, int & noOfSegments)
{  
  //To store the gray version of the image
  Mat gray;
  //To store the thresholded image
  Mat ret;
  //convert the image to grayscale
  cvtColor(image, gray, CV_BGR2GRAY);
  //showImage("Gray Image", gray);
  //threshold the image
  //threshold(gray,ret,0,255,CV_THRESH_BINARY_INV+CV_THRESH_OTSU);
  //showImage("Image after OTSU Thresholding", ret);
  //adaptiveThreshold(gray, ret, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 101, 0);
  adaptiveThreshold(gray, ret, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY_INV, 201, 0);
  showImage("Image after adaptive thresholding", ret);

  //Execute morphological-open
  morphologyEx(ret,ret,MORPH_OPEN,Mat::ones(9,9,CV_8SC1),Point(4,4),2);
  showImage("Thresholded Image after Morphological open",ret);
  
  //get the distance transformation 
  Mat distTransformed(ret.rows,ret.cols,CV_32FC1);
  distanceTransform(ret,distTransformed,CV_DIST_L2,3);
  
  //normalize the transformed image in order to display
  normalize(distTransformed, distTransformed, 0.0, 1, NORM_MINMAX);
  //showImage("Distance Transformation",distTransformed);
  
  //threshold the transformed image to obtain markers for watershed
  threshold(distTransformed,distTransformed,0.1,1,CV_THRESH_BINARY);
  //Renormalize to 0-255 to further calculations
  normalize(distTransformed, distTransformed, 0.0, 255.0, NORM_MINMAX);
  distTransformed.convertTo(distTransformed,CV_8UC1);
  //showImage("Thresholded Distance Transformation", distTransformed);
    
  //calculate the contours of markers
  int compCount = 0;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
  findContours(distTransformed, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

  if( contours.empty() ) {
        return Mat();
  }
    Mat markers(distTransformed.size(), CV_32S);
    markers = Scalar::all(0);
    int idx = 0;
  //draw contours 
    for( ; idx >= 0; idx = hierarchy[idx][0], compCount++ ) {
        drawContours(markers, contours, idx, Scalar::all(compCount+1), -1, 8, hierarchy, INT_MAX);
    }
  
    if( compCount == 0 ) {
        return Mat();
    }

  //calculate the time taken to the watershed algorithm
    double t = (double)getTickCount();
  //apply watershed with the markers as seeds
    watershed( image, markers );
    t = (double)getTickCount() - t;
    printf( "execution time = %gms\n", t*1000./getTickFrequency() );

  //create displayable image of segments
  Mat wshed = createSegmentationDisplay(markers,compCount,image);

  //showImage( "watershed transform", wshed );
  noOfSegments = compCount;

  //returns the segments
  return markers;
}

Mat createSegmentationDisplay(Mat & segments,int numOfSegments,Mat & image)
{
  //create a new image
  Mat wshed(segments.size(), CV_8UC3);
  
  //Create color tab for coloring the segments
  vector<Vec3b> colorTab;
  for(int i = 0; i < numOfSegments; i++ )
  {
      int b = theRNG().uniform(0, 255);
      int g = theRNG().uniform(0, 255);
      int r = theRNG().uniform(0, 255);

      colorTab.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
  }
  
  //assign different color to different segments
  for(int i = 0; i < segments.rows; i++ )
  {
    for(int j = 0; j < segments.cols; j++ )
    {
      int index = segments.at<int>(i,j);
      if( index == -1 )  
        wshed.at<Vec3b>(i,j) = Vec3b(255,255,255);
      else if( index <= 0 || index > numOfSegments )
        wshed.at<Vec3b>(i,j) = Vec3b(0,0,0);
      else
        wshed.at<Vec3b>(i,j) = colorTab[index - 1]; 
    }
  }

  //If the original image available then merge with the colors of segments
  if(image.dims>0) wshed = wshed*0.5+image*0.5;

  return wshed;
}

