\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{float}
\usepackage{subcaption}
% \usepackage[final]{graphicx}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
%\setcounter{page}{4321}
\begin{document}


%%%%%%%%% TITLE
\title{Augmenting Videos with 3D Objects}

\author{Andrei Bajenov\\
{\tt\small abajenov@stanford.edu}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Darshan Kapashi\\
{\tt\small darshank@stanford.edu}
\and
Sagar Chordia\\
{\tt\small sagarc14@stanford.edu}
}


\maketitle




%\thispagestyle{empty}

%%%%%%%%% ABSTRACT

\begin{abstract}
We propose a way to automatically augment a video of a static scene with a 3D object. We use SFM algorithms to estimate the position of the camera. In order to properly support occlusions, we use a novel approach to generate dense depth maps for each frame of the video. We use a combination of semi-global block matching, image segmentation using the watershed algorithm, and planar interpolation to remove noise and sharpen edges. The final result is a video augmented with a 3D object that is properly occluded by objects in front of it. 
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}

The idea of augmenting videos has been around for a while, and we see it everywhere today. One prominent example is CGI in movies, which augments reality with computer generated objects and makes the viewer believe that these objects are part of the environment.
\newline

The process to do this is generally difficult and requires a lot of specialized software and equipment. In this paper, we describe a system that, given an object mesh and a video, allows anyone to place this object seamlessly into the video without any other external inputs.
\newline

There are a number of interesting applications to this. For example, it could be used for seeing how a piece of furniture would look in a room or how a new house would look in a particular location. If integrated with a smartphone's camera, it could also be used when interacting with an environment. For example it could provide navigational pointers, highlight parts of an environment, or even project another person into an environment.
\newline

There are a growing number of technologies that are being built to support this. Project Tango \cite{c1}, by Google, is building a phone that has a built-in depth sensor to make 3D reconstruction and camera tracking easier. Wikitude \cite{c2} is an example of a piece of software that is designed for the purpose of augmenting smartphone videos. It allows for camera tracking and 3D reconstruction. There is also quite a bit of research in 3D reconstruction from a set of 2D images. 
\newline

We have not been able to find a product that specifically takes a video and augments it while supporting occlusions (although a number of technologies like Wikitude support projecting objects based on 
camera positions). This was our main motivation for building this system.

\section{Problem Statement}
We propose a system that puts a 3D object in a video of a static scene while supporting occlusions. We break up the problem into two components:
\begin{enumerate}
\item Estimate camera matrices for each frame of the video, so that we can project an object back into the scene.
\item Obtain sharp and accurate depth maps for each frame, to deal with occlusions.
\end{enumerate}

Below is an example of a scene that we used for testing our system.
\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{2_im2.jpg}
    \caption{An example of a static scene}
\end{figure}

\section{Previous Work}
\subsection{Estimating Camera Positions}
There has been a lot of research done in the area of Structure from Motion (SFM), and there are a number of existing libraries that implement SFM algorithms, including:

\begin{itemize}
\item Theia SFM \cite{c3}
\item Bundler \cite{c12}
\item Visual SFM \cite{c11}
\item OpenCV SFM \cite{c10}
\end{itemize}

We were looking for a few things from the libraries:
\begin{itemize}
\item Camera position estimation
\item Camera parameter estimation
\item Reliable and accurate sparse 3D reconstruction
\end{itemize}

For this project, it was not our goal to try to improve or optimize any of these libraries. We tried a few of them and picked the one that was easiest to use. In our case it was the Theia SFM library.

\subsection{Estimating Depth Map}
A critical part to solving our problem was obtaining accurate and dense depth maps for each frame of the video. There were a number of techniques that we considered:
\begin{itemize}
\item Reconstructing 3D objects using volumetric stereo and using these reconstructions to obtain depth maps \cite{c13}
\item Using a combination of the original images and the sparse 3D points obtained from SFM to approximate the 3D surface positions (using segmentation and planar reconstruction).
\item Using a combination of SFM and stereo matching algorithms to obtain a dense 3D reconstruction of the scene. \cite{c14}
\end{itemize}

While researching volumetric stereo, we found that it was genereally used to get a 3D reconstruction of a single object within a scene. For our purposes, we needed information about the full scene. To extend this algorithm to work on a full scene, we would have needed very reliable image segmentation algorithms that were determenistic between frames. We were not able to find anything that looked promising in this space, so we abandoned this idea.
\newline

We also considered using the sparse 3D points obtained from SFM to approximate a dense 3D reconstruction. We though about partitioning the original image into uniform segments. We would then approximate each segment as a plane in 3D and use the sparse 3D points to estimate these planes. Unfortunately what we found was that we did not have enough points in each image segment to do a planar reconstruction, so we could not use this approach by itself.
\newline

Lastly we found a lot of research about using stereo-matching algorithms to aid in dense 3D reconstruction \cite{c18}.  Specifically, the Middlebury website \cite{c15} contains a lot of submissions and evaluations of many stereo-matching algorithms. We ended up pursuing this approach the most because the research here showed the most promising results.
\newline

To solve our problem, however, we do not need a full 3D reconstruction of the scene. We just need an approximate depth map that has good accuracy around the object boundaries. What we found while using just stereo-matching algorithms was that they were prone to noise. To overcome this problem and achieve the desired results, we propose a novel approach that uses a combination of image segmentation techniques, stereo-matching, and planar interpolation.

%-------------------------------------------------------------------------

\section{Technical Details}
Below we describe our solution. We talk about how we calibrate our camera and run SFM. We then describe our method for getting accurate depth maps. Lastly, we describe how we project 3D objects back into the scene. 

\subsection{Sparse 3D Reconstruction and Camera Matrix Estimation} \label {camera_estimation}

As per standard practice in the camera model used in computer vision, there are 2 parameters:
\begin{itemize}
\item Intrinsic matrix K: A 3x3 matrix which incorporates the focal length and camera center coordinates.
\item Extrinsic matrix [R T]: A 3x4 matrix which maps world coordinates to camera coordinates. R denotes rotation and T denotes translation.
\end{itemize}

The camera transformation is given by a matrix,
$$ M = K[R\ T] $$

It transforms a point in homogeneous world coordinates to homogeneous image coordinates.
\newline

The \textit{point correspondence} problem is defined as follows: Given n images, find points in the images which correspond to the same 3D point. There are several well known algorithms which work reasonably well in practice, for example, SIFT, SURF and DAISY. 
\newline

The \textit{Structure from motion (SFM)} problem is defined as follows: Given m images and n point correspondences, find m camera matrices (M) and n 3D points. Solving the SFM problem for a set of images will give us a sparse 3D reconstruction of the scene.
\newline

To get the intrinsic parameters for our camera we tried a few different approaches:

\begin{itemize}

\item Computing K using single view metrology with 3 vanishing points derived from 3 pairs of mutually orthogonal lines in 3D.

\item Using a checkerboard image to calibrate using OpenCV routines. 

\item Allowing \textit{Structure From Motion (SFM)} algorithms to self-calibrate (which is possible given enough viewpoints of a static scene)

\end{itemize}

Each of these approaches gave us a similar K, so we decided to go with the self-calibration method since it is automatic and we have plenty of views.
\newline

We tried a couple different SFM libraries. The SFM library bundled with OpenCV didn't give good results. The Theia \cite{c3} library was easier to work with and was able to give us fairly accurate sparse reconstructions of a 3D scene, figures \ref{fig:theia1},  \ref{fig:theia2}.
\newline

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{1_theia1.png}
    \caption{Theia's camera positions and a sparse 3D reconstruction}
    \label{fig:theia1}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{1_theia2.png}
    \caption{Side view of the 3D reconstruction}
    \label{fig:theia2}    
\end{figure}

At this point, we have the camera intrinsic and extrinsic matrices as well as a small set of 3D points which can be used to get a sparse reconstruction of the scene.

\subsection{Depth Map Estimation}

A sparse reconstruction is not enough to get a full depth map for each frame. 
\newline

Below we propose a novel approach of using a combination of stereo-matching, image segmentation (using the watershed algorithm), and planar interpolation to get dense 3D depth maps for each frame.

\subsubsection{Terminology}
\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{disparity.png}
    \caption{Stereo setup}
    \label{fig:disparitydepth}
\end{figure}

\begin{enumerate}
\item \textit{Disparity map}. Disparity refers to the difference in image location of an object seen by the left and right cameras, resulting from different positions of two cameras as seen in figure \ref{fig:disparitydepth}. A disparity map is a mapping for each pixel in the image to the disparity value for that pixel. The value represents the distance between the locations of a point in the left and right rectified stereo images. It indicates the relative distance to the camera. A higher value means it is closer to the camera.

\item \textit{Depth map}. A mapping for each pixel in the image to the depth value for that pixel. It indicates how far a point is from the camera. A higher value means it is further from the camera.

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{rectified.png}
    \caption{Image rectification}
    \label{fig:rectification}
\end{figure}

\item \textit{Image rectification}. A transformation to project two images onto a single image plane as seen in figure \ref{fig:rectification}. After rectification, all epipolar lines are parallel in the horizontal axis. All corresponding points have identical vertical coordinates.

\end{enumerate}

\subsubsection{Semi-global block matching}
After obtaining camera matrices for every frame of the image, we use that information to perform stereo matching between pairs of frames. We picked pairs of frames with a good baseline distance between them (in our case around 2.0), and performed stereo matching on rectified versions of these frames.
\newline

We tried different stereo-matching algorithms and picked \textit{Semi-global block matching (SGBM)} since it was readily available in OpenCV and had good performance on the Middlebury dataset. 
\newline

SGBM aims to minimize a global energy function E for the disparity image D, based on the idea of pixel-wise matching of mutual information and approximating a global 2D smoothness constraint by combining
many 1D constraints.
\newline

It takes as input 2 rectified images, taken from the camera on the left and from the camera on the right. It also takes the camera matrix K. It produces disparity maps for the left and right images. Figure \ref{fig:sgbmdisparitymap} shows an example disparity map.

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{4_disparity_unfiltered.png}
    \caption{SGBM Disparity Map}
    \label{fig:sgbmdisparitymap}
\end{figure}

After obtaining a disparity map, we do a first pass to remove noise. We found a technique called \textit{Weighted Least Squares filter} \cite{c10}. The result is in figure  \ref{fig:least_square}.
\newline

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{5_disparity_filtered.png}
    \caption{Weighted Least Squares filtering on an SGBM disparity map}
    \label{fig:least_square}
\end{figure}

\subsubsection{From disparity maps to 3D points}
Disparity maps alone don't help, since they don't tell us the exact depth of objects in each frame. To convert between disparity maps and depth maps, we first need to reproject the disparity values to 3D.
\newline

$p = (x, y)$ is a point in the disparity map. The matrix $Q$ incorporates the transform between the left and right cameras which is obtained during image rectification. We can get the homogeneous coordinate in 3D using this equation.
\newline

$[X\ Y\ Z\ W]^T = Q * [x\ y\ disparity(x, y)\ 1]^T$
\newline

And finally get a mapping from 2D to 3D.
\newline

$3d\_image(x, y) = (X/W, Y/W, Z/W)$
\newline 

Figure \ref{fig:3dreconstruction} is the dense 3D reconstruction obtained from the above equation.

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{6_3D_view_of_depth_map.png}
    \caption{3D reconstruction from a disparity map}
    \label{fig:3dreconstruction}
\end{figure}

\subsubsection{From 3D points to a depth map}
So far, we are able to obtain 3D points from a disparity map. These points are not in world coordinates though, so we need to transform them to world coordinates before generating depth maps.
\newline

Let $x$ be the point in world coordinates. Let $p$ be the point in the original image. We rectify the image for stereo matching. Rectification is a homographic transform. Let $H$ be the inverse of this transform. $K$, $R$ and $T$ are camera parameters.
\newline

The point $x$ maps to $p$ using the camera transform.
\newline

$p = K R x + K T$
\newline

The point $p_r$ is the point in the rectified image, which corresponds to the point $p$ in the original image. $x_r$ is the 3D point in rectified coordinates.
\newline

$p_r = K_r R_r x_r + K_r T_r$
\newline

We get $p$ by applying the rectification transform $H$ on $p_r$,
\newline

$p = H p_r$
\newline

Using these equations, we can derive the equation for point $x$ in original world coordinates.
\newline

$x = R^{-1} K^{-1} H K_r R_r x_r + R^{-1} K^{-1} H K_r T_r - R^{-1} T$
\newline

We reproject these points back into the original frame and compute the depth for each pixel. Figure \ref{fig:depth_from_disparity} is a depth map for the frame from which we generated the disparity map.

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{6_depth_map_from_same_viewpoint.png}
    \caption{Example depth map obtained from a disparity map}
    \label{fig:depth_from_disparity}
\end{figure}

\subsubsection{Missing depth maps}
We don't have a disparity map for every frame. Frames where the camera is moving forward, for example, don't have a good corresponding stereo frame. Rectification between such views introduces too much distortion.
\newline

As such, we need to be able to reconstruct a depth map for any frame, using a depth map generated from some other frame. Fig \ref{fig:another_depth_map} is an example of a depth map viewed from a different camera:

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{7_depth_map_from_another_viewpoint.png}
    \caption{Depth map from another camera location - more occlusions}
    \label{fig:another_depth_map}
\end{figure}

There are far more missing depths, which are mostly there due to occlusions. To help fill in the rest of this depth map, we use a novel technique which combines image segmentation and planar reconstruction, as described in subsections below.
\newline

\subsubsection{Image segmentation}
We use the marker controlled watershed algorithm for image segmentation. The watershed algorithm is based on the concept of flooding the image from its minima and preventing the merging of water coming from different sources. This partitions the image into 2 parts: the catchment basins and the watershed lines. This approach results in over-segmentation, so we use a variant which is based on starting to flood from a set of markers.
\newline

To find the set of markers, we apply the following set of transformations to the image.
In the process of \textit{thresholding} an image, we set the value for each pixel to either $0$ or $1$ based on a threshold. We use adaptive thresholding to the image. It considers local variations in intensity and makes pixels white and black. This is significantly better than using a global threshold because the lighting in the scene is not uniform. Then, the transformed image has several small holes. We use morphological opening to fill in these holes and have a much smaller set of bigger segments. Next, we apply a distance transform, followed by a thresholding transform which gives us a candidate set of markers. Using this, we can apply the watershed algorithm to segment the images.
\newline

This method has several parameters that can be tuned to get a segmentation of desired quality. This includes
\begin{itemize}
\item Window size of adaptive threshold
\item Kernel for the morphological opening
\item Threshold for the distance transform
\end{itemize}

Figure \ref{fig:watershed} is an example of a segmented image.
\newline

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{8_segmentation.png}
    \caption{Image segmented using the watershed algorithm}
    \label{fig:watershed}
\end{figure}

Note that when tuning parameters, our goal is to make sure that segments don't spill between objects. We achieve this by tuning the parameters to generate small segments.

\subsubsection{Planar interpolation}
We have now divided the image into several segments. We assume that each segment is part of a plane. We have the depth map for each 2D image point, which means we have a 3D point corresponding to each 2D point. The camera matrix K has the following structure:
\newline

\[ \left( \begin{array}{ccc}
f_x & 0 & c_x \\
0 & f_y & c_y \\
0 & 0 & 1 \end{array} \right)\] 

For each 2D point, we can compute the corresponding 3D point. $z$ is the depth of this point from the depth map.
\newline

$p = (z * (p_y - c_y) / f_y,\ z  * (p_x - c_x) / f_x,\ z)$
\newline

For each image segment, we collect all the 2D points for which we know a depth ($z$ is not equal to $0$). We get corresponding 3D points. For $n$ points in a segment, we construct a $n$x$3$ matrix $A$ where each row is a 3D point $p = (x, y, z)$. The matrix $t$ is a $n$x$1$ matrix of $-1$. We can use this set of points to estimate a plane using SVD decomposition for the linear system $Ax = t$. This gives us a plane.
\newline

$ax + by + cz + d = 0$
\newline

This plane gives us the depth for every point on it, irrespective of whether we had a depth for it previously from the stereo matching algorithms. This is how we fill up holes in the depth map. We can now trace a ray which starts from the camera and hits the approximate plane. We can compute the length of this line segment and this is the depth of this image point.
\newline

For a point $p = (x, y)$ in the image plane, we can compute the point of intersection between the plane and the ray and compute the depth as
\newline 

$depth = -d / (a * (p_y - c_y) / f_y + b  * (p_x - c_x) / f_x + c)$
\newline

For certain segments, because of measurement noise, poor segmentation, or non-flat surfaces, it is possible to end up with a bad estimate of the plane. We found a simple heuristic to prune these bad planes, $||Ax - t|| > threshold$. This helps to reduce noise in this approach for reconstruction.
\newline

With the above, we get an depth map that looks something like:
\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{9_depth_map_planar_filtering_demo.png}
    \caption{Planar interpolation of image segments}
\end{figure}

For areas where the planes couldn't be reconstructed, we fill those with the original depths, to get:
\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{10_depth_map_planar_filtering_adding_back_missing_planes.png}
    \caption{Filling in missing segments with original depths}
\end{figure}

\subsubsection{Combining depth maps from multiple views}
So far we've dealt with depth maps obtained from a single pair of rectified images. We observed that using a single pair of images doesn't give a full depth map when viewed from different camera angles. Fortunately, in a video sequence, we have many pairs of such images that can help improve results and fill in missing depths.
\newline

To find a depth map for the current frame, we take a number of nearby frames for which we have obtained high-quality depth maps. We deem a depth map to be high quality if it was obtained from a disparity map with a good baseline distance which is not too small and not too large. We then re-project the depth map from those frames into the current view. What we end up with is multiple depths per pixel.
\newline

To pick the desired depth, we sort the depths and pick the first value for which the value between it and the next value is no higher than 15 percent. This is a rudimentary way to pick the smallest z-value cluster. We pick the smallest z-value cluster to eliminate noise and ignore occluded objects.
\newline

With the above approach we obtain a depth map as seen in Fig \ref{fig:depth_map_multiple}. Notice that there is less noise than in previous depth maps, and more pixels have a depth value.

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{11_depth_map_multiple_viewpoints.png}
    \caption{Depth map generated by merging depth maps from multiple viewpoints}
    \label{fig:depth_map_multiple}
\end{figure}

\subsection{Augmenting Video with 3D Objects}
The last step of our system is augmenting the video with a 3D object. At this point we have estimated camera matrices and depth maps. The location of the object within the scene is determined manually, i.e. we take 3D coordinates in the object and translate them such that it is placed behind one of the boxes in the scene. In a real application, you can imagine a user interface which lets you drag and drop the object in the scene. We do not solve this problem here and focus on the mathematical aspects. The object (a bird) is a 3D object. It looks like a 2D blob because we did not add shading to it.
\newline

The 3D object is given as a mesh of triangles. An easy way to augment the frames of the video with this object is to apply the camera transform on each of the vertices and fill in triangles with the object texture. It gets slightly more complex when we want to handle occlusions.
\newline

Since we want to be as accurate as possible, we make sure that the triangles which make up the object are small enough. If they aren't, we can split each triangle into 3 smaller triangles using the centroid and the current vertices. A triangle is visible if all 3 vertices of the triangle are visible. With small enough triangles, this is a good approximation.
\newline

For each vertex $v = [x\ y\ z]$, which is a point in 3d world coordinates, we transform it into camera coordinates point $p = (x, y, z)$ using the camera transform.

$(x, y, z) = [R\ T]\ v$
\newline

The depth of the point is the $z$ coordinate in camera coordinates ($z_p$) and the depth in the frame of the video is as computed using stereo correspondence and planar reconstruction ($z_i$). If the 3d object point is at a greater depth, $z_i < z_p$, it is hidden in the image, otherwise $z_i > z_p$, it is visible.

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{bird_not_occluded.png}
    \caption{Bird is completely visible in this view}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=200px]{bird_half_occluded.png}
    \caption{Bird is half hidden behind the box in this view}
\end{figure}

\section{Evaluation}

As discussed in section \ref{camera_estimation}, we use existing algorithms and libraries to estimate camera parameters in each image of the video. Our main contribution lies in effectively connecting various computer vision algorithms to augment a video. The novelty in this paper is about refining depth maps using image segmentation and approximating a reconstruction using planes. Hence, we will skip evaluation for camera calibration and focus on our method of refining depth maps using planar reconstruction.
\newline

For easy evaluation of individual components of stereo matching algorithms, the computer vision group from Middlebury have designed a stand-alone flexible C++ implementation. \cite{c15} They also provide a collection of datasets and benchmark it against various state-of-art algorithms. The evaluation framework is flexible and supports easy additions of new algorithms. We integrate our methodology and compare the results with other stereo matching algorithms already implemented in the framework.
\newline

We describe the quality metrics we use for
evaluating the performance of various stereo correspondence algorithms
and the techniques we used for acquiring our image
data sets and ground truth estimates. \cite{c19}
\begin{enumerate}
\item RMS (root-mean-squared) error, measured in disparity
units, between the computed disparity map $d_C (x, y)$
and the ground truth map $d_T (x, y)$, i.e.,
$$ R = \bigg( \frac{1}{N}  \sum_{(x,y)}  (|d_C(x,y) - d_T(x,y)|^2) \bigg) ^\frac{1}{2}  $$
where N is the total number of pixels.
\item  Percentage of bad matching pixels, $$ B =  \frac{1}{N}  \sum_{(x,y)}  (|d_C(x,y) - d_T(x,y)| > \delta_d ) $$
\end{enumerate}
where $\delta_d$ is the disparity error tolerance.
\newline

There are various parameters which can be tweaked while evaluating stereo matching in the Middlebury framework. We use default values for most parameters except one parameter. \textit{eval\_bad\_thresh} which controls thresholding to decide whether a pixel is a bad match or not was increased from 1.0 to 5.0. We found 1.0f was too strict and $>$ 90\%  pixels were marked as bad pixels in most images. But a value of 5.0 gives good results for most images. 
\newline

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{.4\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{tsubuka.png} 
    \caption{Image to be evaluated from Middlebury dataset}
    \label{fig:a}
  \end{subfigure}
  \begin{subfigure}{.4\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{tsubuka-truth.png}
    \caption{Ground truth disparity map}
    \label{fig:b}
  \end{subfigure}
  \vfill
  \begin{subfigure}{.4\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{tsubuka-normal-disp.png}
    \caption{Disparity map with Semi-Global Block Matching (SGBM)}
    \label{fig:c}
  \end{subfigure}
  \begin{subfigure}{.4\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{tsubuka-segmentation.png}
    \caption{Image segmentation using watershed transform}
    \label{fig:d}    
  \end{subfigure}
  \vfill
  \begin{subfigure}{.4\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{tsubuka-planar-disparity.png}
    \caption{Filtered disparity map combining SGBM and plane fitting on image segmentation}
    \label{fig:e}    
  \end{subfigure}
  \begin{subfigure}{.4\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{tsukuba_smoothed_disp_more.jpg}
    \caption{Filtered disparity map combining SGBM and aggressive plane fitting on image segmentation}
    \label{fig:f}    
  \end{subfigure}
  \caption{StereoMatch Evaluation using middlebury dataset}
\end{figure*}


Figure \ref{fig:a} shows an image in the Middlebury evaluation dataset. Figure \ref{fig:b} shows the ground-truth disparity map of figure \ref{fig:a}. Disparity map of figure \ref{fig:a} is computed using Semi-Global Block Matching (SGBM) algorithm and is shown in figure \ref{fig:c}. Figure \ref{fig:d} shows image-segmentation on the original image figure \ref{fig:a}. Figure \ref{fig:e} shows refined disparity maps obtained by combining figure \ref{fig:c} and plane fitting on figure \ref{fig:d}. Computed disparity maps figure \ref{fig:c} and figure \ref{fig:e} are compared to the ground truth disparity map figure \ref{fig:b} and the above two metrics are computed. We also compare other algorithms from the Middlebury evaluation framework to our methodology.
\newline

We can control the strictness of plane fitting to refine disparity maps by $norm\_thresh$. When the error of fitting a plane to points of a given image segment is greater than $norm\_thresh$ then we don't refine the disparity map and use the original disparity map. In figure \ref{fig:e} we can see how disparity maps are affected as $norm\_thresh$ value is increased. Figure \ref{fig:e} is bad compared to figure \ref{fig:d} in terms of both metrics and hence it is critical to tune this parameter correctly.
\newline

Sometimes refined disparity maps may not be better than the original disparity map because of bad image segmentation. Figure \ref{fig:16b} is a better approximation to figure \ref{fig:16a} compared to figure \ref{fig:16c}. In figure \ref{fig:16c}, plane fitting on bad image segmentation results in a weird disparity map. So it's important to control the quality of image segmentation to ensure refined disparity maps are better. 
\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{.3\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{sawtooth-truth.png} 
    \caption{True disparity map of sawtooth}
    \label{fig:16a}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{sawtooth-dis.png}
    \caption{Disparity map by SGBM}
    \label{fig:16b}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
    \centering
    \includegraphics[height=0.18\textheight,width=0.8\textwidth]{sawtooth-planar.png}
    \caption{Refined disparity map using image segmentation}
    \label{fig:16c}
  \end{subfigure}
  \caption{Another test image from Middlebury evaluation dataset}
\end{figure*}
\newline

We compute disparity maps for 5 test images in the Middlebury evaluation dataset using our method as well as predefined algorithms in Middlebury. In the following table we report mean values of RMS error and percentage bad pixels. \textbf{normal-SGBM} refers to our SGBM implementation of disparity map. \textbf{planar-SGBM} refers to the filtered disparity map generated by fitting planes using image segmentation. \cite{c19} describes the other algorithms used for encoding. As seen in the table, planar-SGBM performs better than normal-SGBM in both metrics we defined earlier.
\begin{center}
\setlength{\tabcolsep}{4pt}
\renewcommand{\arraystretch}{1.5}
 \begin{tabular}{|c | c | c|} 
 \hline
 Algorithm & Mean RMS error & Mean Bad pixel ratio \\
 \hline\hline
 SSD09bt05 & 1.559039 & 0.024049\\
 \hline 
 SSD09t20 & 1.714662 & 0.030914\\
 \hline 
 SADmf09bt05 & 1.733064 & 0.031358\\
 \hline
 SADmf09t02 & 2.4118 & 0.051564  \\
 \hline
 SAD09t02 & 2.565821 &  0.058292  \\
 \hline
 SADmf09t01 & 3.019650 &  0.084793  \\
 \hline
 planar-SGBM &  3.177850 & 0.071215 \\
 \hline
  normal-SGBM & 3.200869 & 0.072295 \\
 \hline
  SAD09t01 & 3.717277 & 0.135821 \\
 \hline
\end{tabular}
\end{center}

\section{Future Work}
There are still a few things that we would have liked to try to improve our results.
\begin{itemize}
\item Implement state-of-the-art algorithms for stereo-matching and see how they perform with and without planar interpolation. We only had time to add planar interpolation on top of SGBM, but there are better algorithms out there.
\item Look into using the DAISY descriptor instead of stereo-matching for dense 3D reconstruction. See: "A Fast Local Descriptor for Dense Matching" by Tola et. al. \cite{c6}
\item Try different image segmentation approaches to remove spilling out of objects. We started looking into using the canny edge detector to seed the watershed algorithm. \cite{c16}
\item Exploit the fact that we have a video instead of a set of photos to run SFM in real-time.
\item Exploit the fact that we need a depth map of only a small region where the 3D object is projected into the scene (to make our algorithm real-time).
\item Use a better rectification algorithm, as described in "A simple and efficient rectification method for general motion" \cite{c17}
\item \cite{c18} describes how to add better depth map merging techniques - "Metric 3D Surface Reconstruction from Uncalibrated Image Sequences".
\item Project more interesting geometry into the scene. Either by integrating with a ray-tracing library or using OpenGL and taking advantage of its built-in shaders.
\end{itemize}

\section{Conclusion}
In this paper, we proposed a system that takes a 3D object mesh and a video, and augments that video with the object. The system is able to estimate camera positions and generate depth maps for each frame (to support occlusions).
\newline

We used the Theia SFM library to estimate camera positions, and proposed a novel method to estimate depths in each frame. To estimate depths, we used a combination of image segmentation techniques (watershed algorithm), stereo matching (SGBM), and planar interpolation. When compared to stereo matching alone, the combination of these technique allowed us to improve depth map accuracy while at the same time significantly reducing noise and improving sharpness around object boundaries.
\newline

We were able to successfully project a 3D object back into a video. Our algorithm was fully autonomous, and did not require anything other than specifying the object position.
\newline

You can find our code by going to: \texttt{https://bitbucket.org/bajenov1/cs231a/}
\newline

You can find our final augmented video here: \texttt{https://www.youtube.com/watch?v=X37SP4Dihhg}

% References are important to the reader; therefore, each citation must be complete and correct. If at all possible, references should be commonly available publications.
\newpage

%% PUT THIS IN egbib.bib
\begin{thebibliography}{9}
\raggedright
\bibitem{c1} Lee, J. C., and R. Dugan. "Google project tango."
\bibitem{c2} Perry, Simon. "Wikitude: Android app with augmented reality: Mind blowing." digital-lifestyles. info 23.10 (2008).
\bibitem{c3} Sweeney, Christopher, Tobias Hollerer, and Matthew Turk. "Theia: A Fast and Scalable Structure-from-Motion Library." Proceedings of the 23rd Annual ACM Conference on Multimedia Conference. ACM, 2015.
\bibitem{c5} Ravimal Bandara, Image Segmentation using Unsupervised Watershed Algorithm with an Over-segmentation Reduction Technique.
\bibitem{c6} Tola, Engin, Vincent Lepetit, and Pascal Fua. "A fast local descriptor for dense matching." Computer Vision and Pattern Recognition, 2008. CVPR 2008. IEEE Conference on. IEEE, 2008.
\bibitem{c7} Mur-Artal, Raul, J. M. M. Montiel, and Juan D. Tardos. "ORB-SLAM: a versatile and accurate monocular SLAM system." Robotics, IEEE Transactions on 31.5 (2015): 1147-1163.
\bibitem{c8} Furukawa, Yasutaka, and Jean Ponce. "Accurate, dense, and robust multiview stereopsis." Pattern Analysis and Machine Intelligence, IEEE Transactions on 32.8 (2010): 1362-1376.
\bibitem{c9} Kundu, Abhijit, et al. "Joint semantic segmentation and 3d reconstruction from monocular video." Computer Vision–ECCV 2014. Springer International Publishing, 2014. 703-718.
\bibitem{c10} Bradski, Gary, and Adrian Kaehler. Learning OpenCV: Computer vision with the OpenCV library. " O'Reilly Media, Inc.", 2008.
\bibitem{c11} Wu, Changchang. "VisualSFM: A visual structure from motion system." (2011).
\bibitem{c12} Snavely, Noah. "Bundler: Structure from motion (SFM) for unordered image collections." Available online: phototour. cs. washington. edu/bundler/(accessed on 12 July 2013) (2010).
\bibitem{c13} Eisert, Peter. "Reconstruction of Volumetric 3D Models." 3D Videocommunication: Algorithms, Concepts and Real-Time Systems in Human Centred Communication (2005): 133-150.
\bibitem{c15} Scharstein, Damiel, and R. Szeliski. "Middlebury stereo datasets." 2014-04-06]. http://vision, middlebury, edu/stereo/data (2006).
\bibitem{c16} Canny, John. "A computational approach to edge detection." Pattern Analysis and Machine Intelligence, IEEE Transactions on 6 (1986): 679-698.
\bibitem{c20} Haris, Kostas, et al. "Hybrid image segmentation using watersheds and fast region merging." Image Processing, IEEE Transactions on 7.12 (1998): 1684-1699.
\bibitem{c14} Pollefeys, Marc, Reinhard Koch, and Luc Van Gool. "A simple and efficient rectification method for general motion." Computer Vision, 1999. The Proceedings of the Seventh IEEE International Conference on. Vol. 1. IEEE, 1999.
\bibitem{c18} Pollefeys, Marc, et al. "Metric 3D surface reconstruction from uncalibrated image sequences." 3D Structure from Multiple Images of Large-Scale Environments. Springer Berlin Heidelberg, 1998. 139-154.
\bibitem{c19} Scharstein, Daniel, and Richard Szeliski. "A taxonomy and evaluation of dense two-frame stereo correspondence algorithms." International journal of computer vision 47.1-3 (2002): 7-42.
\bibitem{c17} Pollefeys, Marc, Reinhard Koch, and Luc Van Gool. "A simple and efficient rectification method for general motion." Computer Vision, 1999. The Proceedings of the Seventh IEEE International Conference on. Vol. 1. IEEE, 1999.
 
\end{thebibliography}

\end{document}


{\small
\bibliographystyle{ieee}
\bibliography{egbib.bib}
}

\end{document}
