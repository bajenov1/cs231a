preproc_addnoise_stddev 0.000000
preproc_blur_iter 0
frame_ref 2
frame_match 6
disp_min 0
disp_max 19
disp_step 1.000000
disp_n 20
disp_scale 8.000000
match_fn 1
match_interp 3
match_max 1000
match_interval 0
match_interpolated 0
aggr_fn 1
aggr_window_size 7
aggr_iter 0
aggr_minfilter 0
aggr_subpixel 0
aggr_collapse 0
diff_lambda 0.150000
diff_beta 0.500000
diff_scale_cost 0.010000
diff_mu 0.500000
diff_sigmaP 0.400000
diff_epsP 0.010000
opt_fn 2
opt_smoothness 20.000000
opt_grad_thresh 5.000000
opt_grad_penalty 1.000000
opt_occlusion_cost 20
opt_max_iter 100
opt_random 1
opt_sa_var 3
opt_sa_start_T 10.000000
opt_sa_end_T 0.010000
opt_sa_schedule 1
opt_min_margin 0.700000
opt_sym_passes 1
refine_subpix 0
eval_ignore_border 10
eval_bad_thresh 1.000000
eval_error_scale 0.000000
eval_lin_interp 1
eval_disp_gap 2.000000
eval_predict_type 0
eval_textureless_width 3
eval_textureless_thresh 4.000000
eval_discont_width 9
eval_predict_diff 0
eval_empty_color 0x00ffc0ff
eval_partial_shuffle 0.000000
eval_match_quality 0
eval_certain_matches_only 0
rms_error_all 2.296000
rms_error_nonocc 1.941163
rms_error_occ 9.239837
rms_error_textured 1.716473
rms_error_textureless 2.334914
rms_error_discont 1.927155
bad_pixels_all 0.111457
bad_pixels_nonocc 0.100547
bad_pixels_occ 0.692669
bad_pixels_textured 0.085094
bad_pixels_textureless 0.132068
bad_pixels_discont 0.179756
fraction_matched 0.100000
predict_err_near -1.000000
predict_err_middle -1.000000
predict_err_match -1.000000
predict_err_far -1.000000
final_energy -1.000000
total_time -1.000000
verbose 5
evaluate_only 1
output_params results/DPm1o20s0020.txt
depth_map results/DPm1o20s0020.pgm
