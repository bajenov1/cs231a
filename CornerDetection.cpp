#include "CornerDetection.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <iostream>

namespace corner {
using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;

/** @function cornerHarris_demo */
void cornerHarris_detect(Mat src, Mat src_gray, Mat& corners, int thresh)
{

  Mat dst, dst_norm, dst_norm_scaled;
  dst = Mat::zeros(src.size(), CV_32FC1);

  /// Detector parameters
  int blockSize = 2;
  int apertureSize = 3;
  double k = 0.02;

  /// Detecting corners
  cornerHarris( src_gray, dst, blockSize, apertureSize, k, BORDER_DEFAULT );


  /// Normalizing
  normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );

  //convertScaleAbs( dst_norm, dst_norm_scaled);
  corners = dst_norm;
  //cout << "dst_norm_scaled = "<< endl << " "  << dst_norm_scaled << endl << endl;
}

bool detectHarrisKeyPoints(Mat& dst_norm, Mat& dst_norm_scaled, int thresh , float scale_factor) {
/// Drawing a circle around corners
  std::vector<cv::KeyPoint> keypoints;
  int detected = 0;
  int ws = 5;
    for( int j = ws; j < dst_norm.rows-ws ; j++ )
       { for( int i = ws; i < dst_norm.cols-ws; i++ )
            {
              float sum_dst_norm = 0;
              int count_cell = 0;
              for (int m = -ws; m<ws; m++) {
                for (int n = -ws; n<ws;n++) {
                  sum_dst_norm += dst_norm.at<float>(j+m, i+n);
                  count_cell++;
                }
              }
              float avg_dst_norm = (sum_dst_norm - dst_norm.at<float>(j,i))/ (count_cell-1);
              int dst_value = dst_norm.at<float>(j,i);
              if( dst_value > scale_factor * avg_dst_norm && dst_value > thresh)
                {
                  //keypoints.push_back(KeyPoint(Point2f(i,j),2));
                  circle(dst_norm_scaled, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
                  detected++;
                }
            }
       }
  //cv::drawKeypoints(dst_norm_scaled, keypoints, dst_norm_scaled);
  auto s = dst_norm.size();
  if (detected > (s.width * s.height / 10)) {
    return false;
  }
  return true;
}

void shiTomasiDetector(Mat& src, Mat& copy, int maxCorners)
{
  if( maxCorners < 1 ) { maxCorners = 1; }

  /// Parameters for Shi-Tomasi algorithm
  vector<Point2f> corners;
  double qualityLevel = 0.05; // 0.01
  double minDistance = 40; // 10
  int blockSize = 3;
  bool useHarrisDetector = false;
  double k = 0.04; // 0.04

  /// Copy the source image

  copy = src.clone();

  /// Apply corner detection
  goodFeaturesToTrack( src,
               corners,
               maxCorners,
               qualityLevel,
               minDistance,
               Mat(),
               blockSize,
               useHarrisDetector,
               k );


  /// Draw corners detected
  cout<<"** Number of corners detected: "<<corners.size()<<endl;
  int r = 4;
  for( int i = 0; i < corners.size(); i++ )
    //Scalar(rng.uniform(0,255), rng.uniform(0,255),
             // rng.uniform(0,255))
     { circle( copy, corners[i], r, Scalar(0), -1, 8, 0 ); }
}

}
