#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <iostream>
#include "opencv2/stitching/detail/motion_estimators.hpp"
#include "CornerDetection.cpp"

#include <opencv2/core.hpp>
//#include <opencv2/sfm.hpp>
//#include <opencv2/viz.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <sstream>

using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;


int main( int argc, char** argv )
{
    VideoCapture cap(argv[1]);
    if (!cap.isOpened())
    {
        return -1;
    }

    namedWindow("gray", 1);

    Mat frame;
    Mat gray;
    int frameNumber = 0;
    vector<KeyPoint> k1;
    Mat d1;
    int i =0;
    for(;;)
    {
        // Get frame
        cap >> frame;
        if (frame.rows == 0) {
            break;
        }
        stringstream ss;
        ss <<"chess" << i << ".jpg";
        i++;
        auto done = imwrite("../media/chessSequence/"+ss.str(), frame);
        if (!done) {
          cout <<"fail to write image";
        }

    }
    return 0;
}
