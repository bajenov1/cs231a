#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>

using namespace cv;
using namespace std;

string imgfile1 = "../media/sequence/blah0.jpg";
string imgfile2 = "../media/sequence/blah4.jpg";

Mat computeDaisy(InputArray img);

void showImage(string const& name, InputArray img, double p = 1) {
  namedWindow(name, WINDOW_NORMAL);
  Mat small_img;
  resize(img, small_img, Size(), p, p);
  imshow(name, small_img);
  waitKey(0);
}

Mat resize(InputArray img, double p) {
  Mat small_img;
  resize(img, small_img, Size(), p, p);
  return small_img;
}
/*
int main() {
  string filename = "../media/room.jpg";
  // Read the file
  Mat image = imread(filename, CV_LOAD_IMAGE_COLOR);  
  cout << "image: " << image.rows << " " << image.cols << endl;

  Mat desc = computeDaisy(image);
  cout << "descriptor: " << desc.rows << " " << desc.cols << endl;
  return 0;
}

Mat computeDaisy(InputArray img) {
	Ptr<xfeatures2d::DAISY> f2d = xfeatures2d::DAISY::create();
	Mat desc;
	f2d->compute(img, desc);
	return desc;
}

vector<pair<Vec2i, Vec2i>> matchDaisyDescriptors(InputArray i1, InputArray d1, InputArray i2, InputArray d2) {
	vector<pair<Vec2i, Vec2i>> matches;
	return matches;
}
*/

const float nn_match_ratio = 0.7f;      // Nearest neighbor matching ratio
const float keypoint_diameter = 15.0f;

int main(int argc, char ** argv){

  // Load images
  cout << "Load images" << endl;
  Mat img1 = imread(imgfile1);
  cout << "image1: " << img1.rows << " " << img1.cols << endl;
  Mat img2 = imread(imgfile2);
  cout << "image2: " << img2.rows << " " << img2.cols << endl;

  img1 = resize(img1, 0.5);
  img2 = resize(img2, 0.5);

  cout << "Create keypoint vectors" << endl;
  vector<KeyPoint> keypoints1, keypoints2;

  // Add every pixel to the list of keypoints for each image
  for (float x = keypoint_diameter; x < img1.size().width - keypoint_diameter; x++) {
    for (float y = keypoint_diameter; y < img1.size().height - keypoint_diameter; y++) {
      keypoints1.push_back(KeyPoint(x, y, keypoint_diameter));
      keypoints2.push_back(KeyPoint(x, y, keypoint_diameter));
    }
  }

  Mat desc1, desc2;
  Ptr<xfeatures2d::DAISY> descriptor_extractor = cv::xfeatures2d::DAISY::create();
  // Compute DAISY descriptors for both images 
  cout << "Compute daisy image 1" << endl;
  descriptor_extractor->compute(img1, keypoints1, desc1);
  cout << "Compute daisy image 2" << endl;
  descriptor_extractor->compute(img2, keypoints2, desc2);

  cout << "Compute matches" << endl;
  vector <vector<DMatch>> matches;
  // For each descriptor in image1, find 2 closest matched in image2
  FlannBasedMatcher flannmatcher;
  flannmatcher.add(desc1);
  flannmatcher.train();
  flannmatcher.knnMatch(desc2, matches, 2);


  cout << "Find good matches" << endl;
  // ignore matches with high ambiguity -- i.e. second closest match not much worse than first
  // push all remaining matches back into DMatch Vector "good_matches" so we can draw them using DrawMatches
  int                 num_good = 0;
  vector<KeyPoint>    matched1, matched2; 
  vector<DMatch>      good_matches;

  for (int i = 0; i < matches.size(); i++) {
    DMatch first  = matches[i][0];
    DMatch second = matches[i][1];

    if (first.distance < nn_match_ratio * second.distance) {
      matched1.push_back(keypoints1[first.trainIdx]);
      matched2.push_back(keypoints2[first.queryIdx]);
      good_matches.push_back(DMatch(num_good, num_good, 0));
      num_good++;
    }
  }

  cout << "Show images" << endl;
  Mat res;
  drawMatches(img1, matched1, img2, matched2, good_matches, res);
  showImage("Result", res);
  return 0;
}