#include <pcl_visualization/cloud_viewer.h>

#include <fstream>

void foo () {
	pcl::PointCloud<pcl::PointXYZ> cloud;
	//... populate cloud

	ifstream points_file("../media/points.txt");
	float x, y, z;
	while (points_file >> x >> y >> z) {
    	cloud.push_back(PointXYZ(x, y, z));
	}

	pcl_visualization::CloudViewer viewer("Simple Cloud Viewer");
	viewer.showCloud(cloud);
	while (!viewer.wasStopped()) {
	}
}

int main() {
	foo();
	return 0;
}