// Given a set of points, find the plane they all lie on
// points: N x 3 matrix of points
Vec4f computePlane(InputArray points) {
	int N = points.cols;
	Mat centroid;

	// Compute the centroid
	reduce(points, centroid, 0, CV_REDUCE_SUM);
	centroid = centroid / N;

	// Repeat the centroid
	Mat centroid_mat;
	repeat(centroid, 0, N, centroid_mat);

	// Subtract the centroid from the points
	Mat normalized_points = points - centroid_mat;

	// ax + by + cz + 1 = 0
	b = -1 * Mat::ones(N, 1);
	A = normalized_points;

	// Solve using svd
	Mat solution; // 3 x 1
	bool success = solve(A, b, solution);
	if (!success) {
		throw runtime_error("Could not solve Ax=b in computePlane()");
	}

	Vec4f plane;
	plane[0] = solution.at(0, 0);
	plane[1] = solution.at(1, 0);
	plane[2] = solution.at(2, 0);
	plane[3] = 1;
	return plane;
}